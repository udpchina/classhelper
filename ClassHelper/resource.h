//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ClassHelper.rc
//
#define IDD_CLASSHELPER_DIALOG          102
#define IDR_MAINFRAME                   128
#define IDC_BUTTON_NEWCLASS             1001
#define IDC_BUTTON_DELETE_CLASS         1010
#define IDC_EDIT_MIN_SPEED              1014
#define IDC_EDIT_ClASSNAME              1016
#define IDC_EDIT_MIN_AREA               1018
#define IDC_EDIT_MAX_AREA               1019
#define IDC_EDIT_MAX_SPEED              1020
#define IDC_COMBO_COLOR                 1021
#define IDC_BUTTON1                     1022
#define IDC_BUTTON_DELALLCLASS          1022
#define IDC_LIST_CLASS                  1023
#define IDC_IPADDRESS2                  1051
#define IDC_IPADDRESS                   1051
#define IDC_EDIT2                       1052
#define IDC_EDIT_PORT                   1052
#define IDC_EDIT3                       1053
#define IDC_EDIT_USERNAME               1053
#define IDC_EDIT4                       1054
#define IDC_EDIT_PASSWORD               1054
#define IDC_CHECK1                      1058
#define IDC_CHECK_SSL                   1058
#define IDC_RADIO4                      1059
#define IDC_RADIO_NVCIPE                1059
#define IDC_RADIO5                      1060
#define IDC_RADIO_IPNIPX                1060
#define IDC_BUTTON2                     1061
#define IDC_BTN_DOWNLOAD                1061
#define IDC_BUTTON3                     1062
#define IDC_BTN_UPLOAD                  1062
#define IDC_BUTTON4                     1063
#define IDC_BTN_MOVEUP                  1063
#define IDC_BUTTON5                     1064
#define IDC_BTN_MOVEDOWN                1064
#define IDC_COMBO2                      1065
#define IDC_COMBO_ENABLE                1065

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1066
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
