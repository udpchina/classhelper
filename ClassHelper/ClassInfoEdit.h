#pragma once


// CClassInfoEdit

class CClassInfoEdit : public CEdit
{
	DECLARE_DYNAMIC(CClassInfoEdit)

public:
	CClassInfoEdit();
	virtual ~CClassInfoEdit();

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
};


