#pragma once

#include <afxtempl.h>	// Used for CArray

// CComboColor

class CComboColor : public CComboBox
{
	DECLARE_DYNAMIC(CComboColor)

public:
	CComboColor();
	virtual ~CComboColor();

protected:
	DECLARE_MESSAGE_MAP()
public:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	virtual void PreSubclassWindow();
public:
	CArray<COLORREF, COLORREF> m_ColorsHighligh;
	void InitColor(COLORREF *pColor, int ColorCount);
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
};


