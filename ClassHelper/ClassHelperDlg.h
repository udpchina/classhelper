
// ClassHelperDlg.h : header file
//
#pragma once

#include "ClassInfoEdit.h"
#include "ComboColor.h"
#include "Lib\CHttpClient.hpp"

#define MAXZONECOLORS 7
#define PIXELS_PER_AREA_UNIT 60
#define PIXELS_PER_SPEED_UNIT 6
#define MAX_NUM_CLASS_COUNT 20
#define ID_INVALID		-1
#define MAX_NUM_AREAFILTERS 5
#define _VCA_CGI_ _T("/nvc-cgi/admin/vca.cgi")


typedef struct tag_ClassScope
{
	float MinArea;
	float MaxArea;
	INT32 MinSpeed;
	INT32 MaxSpeed;
} ClassScope;

typedef struct tag_ClassInfo
{	
	INT32 ClassID;
	INT32 ColorID;
	TCHAR Name[100];
	INT32 bEnable;
	INT32 Postion;
	ClassScope Scope; 
	struct tag_ClassInfo *Pre;
	struct tag_ClassInfo *Next;
} ClassInfo;

// CClassHelperDlg dialog
class CClassHelperDlg : public CDialogEx
{
// Construction
public:
	CClassHelperDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_CLASSHELPER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
private:
	INT32 xv2a(float x);//virtual x value to absolute x value
	INT32 yv2a(INT32 y);//virtual y value to absolute y value
	float xa2v(INT32 x);//absolute x value to virtual x value
	INT32 ya2v(INT32 x);//absolute y value to virtual y value
// Member
private:
	CPoint	m_OriginPoint;
	static TCHAR* m_ColorName[MAXZONECOLORS];
	static COLORREF m_ColorLUT[MAXZONECOLORS]; 

	typedef enum tag_eSelPos
	{
		Pos_NONE = 0,
		Pos_LeftTop = 1,
		Pos_LeftBottom = 2,
		Pos_RightBottom = 3,
		Pos_RightTop = 4,
		Pos_IN = 5		
	} eSelPos;

	typedef struct tag_ClassBaseInfo
	{
		ClassScope BaseScope;
		CPoint BasePoint; 
		eSelPos SelPos;
	} ClassBaseInfo;

	typedef enum tag_ClassStatus
	{
		Cls_NotOnBoard = 0,
		Cls_OnBoard = 1,
		Cls_Updated = 2,
		Cls_Added = 3,
		Cls_Removed = 4
	} ClassStatus;

	ClassInfo *m_pHeadClass;
	ClassInfo *m_pOriClassList;
	ClassInfo *m_pCurSelClass;
	INT32	m_CurSelClassID;
	CClassInfoEdit m_EditCtrlMinArea;
	CClassInfoEdit m_EditCtrlMaxArea;
	CClassInfoEdit m_EditCtrlMinSpeed;
	CClassInfoEdit m_EditCtrlMaxSpeed;
	ClassBaseInfo m_SelClaseBase;
	CEdit m_EditCtrlClassName;
	CListBox m_ListClass;
	INT32 m_CurMaxClassID;
	INT32 m_TotalClass;
	CString m_WebServerIP;
	INT32 m_Port;
	CString m_Username;
	CString m_Password;
	BOOL m_bSSL;
	INT32 m_DeviceType;

private:
	void DrawCoordinate(CPaintDC *pDc);
	void DrawClass(CPaintDC *pDc, ClassInfo *pClassInfo);
	BOOL AppendClassToList(ClassInfo* &pHead, ClassInfo *pCI, BOOL bOriClsList = FALSE);
	BOOL UpdateClassToList(ClassInfo *pCurSelClass, ClassInfo *pCI);
	void DeleteSelClassFromList();
	void DrawClassCount(CPaintDC *pDc);
	void ClearClassList(ClassInfo* &pHead);
	BOOL FindSelClass(CPoint Point);
	ClassInfo* FindClassPostion(ClassInfo* pHead, INT32 Postion);
	ClassInfo* FindClassID(ClassInfo* pHead, INT32 ID);
	void NormalizationClassInfo(ClassInfo *pCI);
	void UpdateCtrlStatus();
	INT32 SetCurSelClassID();   
	INT32 AllocClassID();
	BOOL RetrieveClassInfro(struct tLL & tLLTop);
	BOOL pcgi_ismatch( char* _psz1, char* _psz2 );
	BOOL _request_cgicommand(LPTSTR _lpszcgi, LPTSTR _lpszoption, BYTE* _lpcbresponse, DWORD* _pdwlength );
public:
	afx_msg void OnBnClickedButtonNewclass();
	afx_msg void OnKillfocusEditMinArea();
	void DrawAllClass(CPaintDC *pDc);
public:
	CComboColor m_ComboColor;
	CButton m_BtnNewClass;
	CButton m_BtnMoveUp;
	CButton m_BtnMoveDown;
	CComboBox m_ComboEnable;
	CIPAddressCtrl m_IPaddr;
public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnClickedButtonDeleteClass();
	afx_msg void OnClickedButtonDelallclass();
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnSelchangeComboColor();
	afx_msg void OnKillfocusEditMaxArea();
	afx_msg void OnKillfocusEditMinSpeed();
	afx_msg void OnKillfocusEditMaxSpeed();
	afx_msg void OnKillfocusEditClassname();
	afx_msg void OnSelchangeListClass();
	afx_msg void OnClickedBtnDownload();
	afx_msg void OnClickedBtnUpload();
	afx_msg void OnClickedBtnMoveup();
	afx_msg void OnClickedBtnMovedown();

	afx_msg void OnClickedCheckSsl();
};
