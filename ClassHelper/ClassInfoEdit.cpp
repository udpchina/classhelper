// ClassInfoEdit.cpp : implementation file
//

#include "stdafx.h"
#include "ClassHelper.h"
#include "ClassInfoEdit.h"


// CClassInfoEdit

IMPLEMENT_DYNAMIC(CClassInfoEdit, CEdit)

CClassInfoEdit::CClassInfoEdit()
{

}

CClassInfoEdit::~CClassInfoEdit()
{
}


BEGIN_MESSAGE_MAP(CClassInfoEdit, CEdit)
//	ON_WM_KEYDOWN()
	ON_WM_CHAR()
END_MESSAGE_MAP()


// CClassInfoEdit message handlers


void CClassInfoEdit::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	// TODO: Add your message handler code here and/or call default
	if (((TCHAR)nChar >= '0' && (TCHAR)nChar <= '9') || ((TCHAR)nChar == '\b'))
	{
		CEdit::OnChar(nChar, nRepCnt, nFlags);
	}
	else if ((TCHAR)nChar == '.')
	{
		CString str;
		int nPos = 0;
		this->GetWindowText(str);
		nPos = str.Find('.');
		if(nPos >= 0)
			return;
		CEdit::OnChar(nChar, nRepCnt, nFlags);
	}

}
