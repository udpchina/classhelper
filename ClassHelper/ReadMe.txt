================================================================================
    ClassHelper Project Overview
===============================================================================

This application is a tool used to simplify the process of customize classification configuration. 
User can download the classification information from UDP device, configure them in 
this tool, and then upload the modified information back to device.