
// ClassHelperDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ClassHelper.h"
#include "ClassHelperDlg.h"
#include "afxdialogex.h"
#include "atlconv.h"
#include <math.h>
#include "lib\helper_parsing.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CClassHelperDlg dialog
TCHAR* CClassHelperDlg::m_ColorName[MAXZONECOLORS] = { _T("Red"), _T("Green"), _T("Blue"), _T("Yellow"), _T("Fuchsia"), _T("Aqua"), _T("Orange")};
COLORREF CClassHelperDlg::m_ColorLUT[MAXZONECOLORS] = 
{
	RGB(255,128,128),
	RGB(0,128,0),
	RGB(0,128,255),
	RGB(255,128,64),
	RGB(64,128,128),
	RGB(63,72,204),
	RGB(255,165,0),		
};


CClassHelperDlg::CClassHelperDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CClassHelperDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CClassHelperDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	//	DDX_Control(pDX, IDC_COMBO_COLOR, m_ComboColor);
	DDX_Control(pDX, IDC_BUTTON_NEWCLASS, m_BtnNewClass);
	DDX_Control(pDX, IDC_LIST_CLASS, m_ListClass);
	DDX_Control(pDX, IDC_BTN_MOVEUP, m_BtnMoveUp);
	DDX_Control(pDX, IDC_BTN_MOVEDOWN, m_BtnMoveDown);
	DDX_Control(pDX, IDC_COMBO_ENABLE, m_ComboEnable);
	DDX_Control(pDX, IDC_IPADDRESS, m_IPaddr);
}

BEGIN_MESSAGE_MAP(CClassHelperDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_NEWCLASS, &CClassHelperDlg::OnBnClickedButtonNewclass)
	ON_EN_KILLFOCUS(IDC_EDIT_MIN_AREA, &CClassHelperDlg::OnKillfocusEditMinArea)
	ON_WM_LBUTTONDOWN()
	ON_BN_CLICKED(IDC_BUTTON_DELETE_CLASS, &CClassHelperDlg::OnClickedButtonDeleteClass)
	ON_BN_CLICKED(IDC_BUTTON_DELALLCLASS, &CClassHelperDlg::OnClickedButtonDelallclass)
	ON_WM_LBUTTONDBLCLK()
	ON_WM_MOUSEMOVE()
	ON_CBN_SELCHANGE(IDC_COMBO_COLOR, &CClassHelperDlg::OnSelchangeComboColor)
//	ON_EN_CHANGE(IDC_EDIT_MIN_AREA, &CClassHelperDlg::OnChangeEditMinArea)
ON_EN_KILLFOCUS(IDC_EDIT_MAX_AREA, &CClassHelperDlg::OnKillfocusEditMaxArea)
ON_EN_KILLFOCUS(IDC_EDIT_MIN_SPEED, &CClassHelperDlg::OnKillfocusEditMinSpeed)
ON_EN_KILLFOCUS(IDC_EDIT_MAX_SPEED, &CClassHelperDlg::OnKillfocusEditMaxSpeed)
ON_EN_KILLFOCUS(IDC_EDIT_ClASSNAME, &CClassHelperDlg::OnKillfocusEditClassname)
//ON_EN_CHANGE(IDC_EDIT_ClASSNAME, &CClassHelperDlg::OnChangeEditClassname)
//ON_EN_UPDATE(IDC_EDIT_ClASSNAME, &CClassHelperDlg::OnUpdateEditClassname)
ON_LBN_SELCHANGE(IDC_LIST_CLASS, &CClassHelperDlg::OnSelchangeListClass)
ON_BN_CLICKED(IDC_BTN_DOWNLOAD, &CClassHelperDlg::OnClickedBtnDownload)
ON_BN_CLICKED(IDC_BTN_UPLOAD, &CClassHelperDlg::OnClickedBtnUpload)
ON_BN_CLICKED(IDC_BTN_MOVEUP, &CClassHelperDlg::OnClickedBtnMoveup)
ON_BN_CLICKED(IDC_BTN_MOVEDOWN, &CClassHelperDlg::OnClickedBtnMovedown)
ON_BN_CLICKED(IDC_CHECK_SSL, &CClassHelperDlg::OnClickedCheckSsl)
END_MESSAGE_MAP()


// CClassHelperDlg message handlers

BOOL CClassHelperDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	CRect	ClientRect;
	GetClientRect(&ClientRect);
	m_OriginPoint.x = ClientRect.left + 30;
	m_OriginPoint.y = ClientRect.bottom * 5/6;
	m_pHeadClass = NULL;
	m_pOriClassList = NULL;
	m_pCurSelClass = NULL;
	m_CurMaxClassID = 0;
	m_TotalClass = 0;

	m_IPaddr.SetAddress(192, 168, 1, 208);
	((CEdit*)GetDlgItem(IDC_EDIT_PORT))->SetWindowText(_T("80"));
	((CEdit*)GetDlgItem(IDC_EDIT_USERNAME))->SetWindowText(_T("root"));
	((CEdit*)GetDlgItem(IDC_EDIT_PASSWORD))->SetWindowText(_T("pass"));

	m_EditCtrlMinArea.SubclassDlgItem(IDC_EDIT_MIN_AREA, this);
	m_EditCtrlMaxArea.SubclassDlgItem(IDC_EDIT_MAX_AREA, this);
	m_EditCtrlMinSpeed.SubclassDlgItem(IDC_EDIT_MIN_SPEED, this);
	m_EditCtrlMaxSpeed.SubclassDlgItem(IDC_EDIT_MAX_SPEED, this);
	m_EditCtrlClassName.SubclassDlgItem(IDC_EDIT_ClASSNAME, this);

	m_ComboEnable.SetCurSel(1);
	m_ComboColor.SubclassDlgItem(IDC_COMBO_COLOR, this);
	m_ComboColor.InitColor(m_ColorLUT, MAXZONECOLORS);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CClassHelperDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		INT32 cxIcon = GetSystemMetrics(SM_CXICON);
		INT32 cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		INT32 x = (rect.Width() - cxIcon + 1) / 2;
		INT32 y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{		
		CPaintDC Dc(this);
		DrawCoordinate(&Dc);
		DrawAllClass(&Dc);
		DrawClassCount(&Dc);
		UpdateCtrlStatus();
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CClassHelperDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


//virtual x value to absolute x value
INT32 CClassHelperDlg::xv2a(float x)
{
	float AbsoluteX;

	AbsoluteX = x * PIXELS_PER_AREA_UNIT + m_OriginPoint.x;
	return (INT32)AbsoluteX;
}


INT32 CClassHelperDlg::yv2a(INT32 y)
{
	INT32 AbsoluteY;

	AbsoluteY = m_OriginPoint.y - y *PIXELS_PER_SPEED_UNIT;
	return AbsoluteY;
}

//absolute x value to virtual x value
float CClassHelperDlg::xa2v(INT32 x)
{
	float VirtualX;
	VirtualX =	((float)x - (float)m_OriginPoint.x)/ (float)PIXELS_PER_AREA_UNIT;
	return VirtualX;
}

INT32 CClassHelperDlg::ya2v(INT32 y)
{
	INT32 VirtualY;

	VirtualY = (INT32)((m_OriginPoint.y - y)/PIXELS_PER_SPEED_UNIT);
	return VirtualY;
}

void CClassHelperDlg::DrawCoordinate(CPaintDC *pDc)
{
	CPen *pOldPen, BoldPen, ThinPen;
	CFont font,*pOldFont;
	
	font.CreateFont(15,
					0,
					0,
					0,
					FW_NORMAL,
					false,
					false,
					false,
					ANSI_CHARSET,
					OUT_DEFAULT_PRECIS,
					CLIP_DEFAULT_PRECIS,
					DEFAULT_QUALITY,
					DEFAULT_PITCH|FF_DONTCARE,
					_T("Arial"));

	BoldPen.CreatePen(PS_SOLID,2,RGB(0,0,0));
	ThinPen.CreatePen(PS_SOLID,1,RGB(0,0,0));

	pOldFont=pDc->SelectObject(&font);
	pOldPen=pDc->SelectObject(&BoldPen); 
	//x y axis
	pDc->MoveTo(xv2a(0), yv2a(0));
	pDc->LineTo(xv2a(0), yv2a(75));
	pDc->MoveTo(xv2a(0), yv2a(0));
	pDc->LineTo(xv2a(8.5), yv2a(0));

	//y direction arrow
	pDc->MoveTo(xv2a(-0.01), yv2a(75));
	pDc->LineTo(xv2a(-0.06), yv2a(73));
	pDc->MoveTo(xv2a(0), yv2a(75));
	pDc->LineTo(xv2a(0.05), yv2a(73));

	//x direction arrow
	pDc->MoveTo(xv2a(8.5), yv2a(0));
	pDc->LineTo(xv2a(8.3), yv2a(1)+2);
	pDc->MoveTo(xv2a(8.5), yv2a(0));
	pDc->LineTo(xv2a(8.3), yv2a(-1)-2);

	pDc->SelectObject(&ThinPen);
	pDc->SetBkMode(TRANSPARENT);
	//origin point
	pDc->TextOut(xv2a(-0.2), yv2a(-1), _T("0"));
	//x-axis scale
	pDc->MoveTo(xv2a(1), yv2a(0));
	pDc->LineTo(xv2a(1), yv2a(-1));
	pDc->TextOut(xv2a(0.95), yv2a(-2), _T("1"));
	pDc->MoveTo(xv2a(2), yv2a(0));
	pDc->LineTo(xv2a(2), yv2a(-1));
	pDc->TextOut(xv2a(1.95), yv2a(-2), _T("2"));
	pDc->MoveTo(xv2a(3), yv2a(0));
	pDc->LineTo(xv2a(3), yv2a(-1));
	pDc->TextOut(xv2a(2.95), yv2a(-2), _T("3"));
	pDc->MoveTo(xv2a(4), yv2a(0));
	pDc->LineTo(xv2a(4), yv2a(-1));
	pDc->TextOut(xv2a(3.95), yv2a(-2), _T("4"));
	pDc->MoveTo(xv2a(5), yv2a(0));
	pDc->LineTo(xv2a(5), yv2a(-1));
	pDc->TextOut(xv2a(4.95), yv2a(-2), _T("5"));
	pDc->MoveTo(xv2a(6), yv2a(0));
	pDc->LineTo(xv2a(6), yv2a(-1));
	pDc->TextOut(xv2a(5.95), yv2a(-2), _T("6"));
	pDc->MoveTo(xv2a(7), yv2a(0));
	pDc->LineTo(xv2a(7), yv2a(-1));
	pDc->TextOut(xv2a(6.95), yv2a(-2), _T("7"));
	pDc->MoveTo(xv2a(8), yv2a(0));
	pDc->LineTo(xv2a(8), yv2a(-1));
	pDc->TextOut(xv2a(7.95), yv2a(-2), _T("8"));
	pDc->TextOut(xv2a(8.2), yv2a(-2), _T("Area"));

	//y-axis scale
	pDc->MoveTo(xv2a(0), yv2a(10));
	pDc->LineTo(xv2a(-0.1), yv2a(10));
	pDc->TextOut(xv2a(-0.4), yv2a(11), _T("10"));
	pDc->MoveTo(xv2a(0), yv2a(20));
	pDc->LineTo(xv2a(-0.1), yv2a(20));
	pDc->TextOut(xv2a(-0.4), yv2a(21), _T("20"));
	pDc->MoveTo(xv2a(0), yv2a(30));
	pDc->LineTo(xv2a(-0.1), yv2a(30));
	pDc->TextOut(xv2a(-0.4), yv2a(31), _T("30"));
	pDc->MoveTo(xv2a(0), yv2a(40));
	pDc->LineTo(xv2a(-0.1), yv2a(40));
	pDc->TextOut(xv2a(-0.4), yv2a(41), _T("40"));
	pDc->MoveTo(xv2a(0), yv2a(50));
	pDc->LineTo(xv2a(-0.1), yv2a(50));
	pDc->TextOut(xv2a(-0.4), yv2a(51), _T("50"));
	pDc->MoveTo(xv2a(0), yv2a(60));
	pDc->LineTo(xv2a(-0.1), yv2a(60));
	pDc->TextOut(xv2a(-0.4), yv2a(61), _T("60"));
	pDc->MoveTo(xv2a(0), yv2a(70));
	pDc->LineTo(xv2a(-0.1), yv2a(70));
	pDc->TextOut(xv2a(-0.4), yv2a(71), _T("70"));
	pDc->TextOut(xv2a(0.2), yv2a(73), _T("Speed"));

	pDc->SelectObject(pOldFont);
	pDc->SelectObject(pOldPen);
	BoldPen.DeleteObject();
	ThinPen.DeleteObject();
	font.DeleteObject();
}

void CClassHelperDlg::DrawClass(CPaintDC *pDc, ClassInfo *pClassInfo)
{ 
	CRect ClassScope;
	
	ClassScope.left = xv2a(pClassInfo->Scope.MinArea);
	ClassScope.right = xv2a(pClassInfo->Scope.MaxArea);
	ClassScope.bottom = yv2a(pClassInfo->Scope.MinSpeed);
	ClassScope.top = yv2a(pClassInfo->Scope.MaxSpeed);

	pDc->TextOut(ClassScope.left + 1, ClassScope.top - 20, pClassInfo->Name);
	CString str;
	str.Format(_T("Pos:%d ID:%d"), pClassInfo->Postion, pClassInfo->ClassID);
	pDc->TextOut(ClassScope.left + 1, ClassScope.top - 10, str);

	CBrush ClassColor(m_ColorLUT[pClassInfo->ColorID]);
	pDc->FillRect(&ClassScope, &ClassColor);
}

void CClassHelperDlg::NormalizationClassInfo(ClassInfo *pCI)
{	
	INT32 iTmp;
	float fTmp;

	if (pCI->Scope.MinArea > pCI->Scope.MaxArea)
	{	
		fTmp = pCI->Scope.MinArea;
		pCI->Scope.MinArea = pCI->Scope.MaxArea;
		pCI->Scope.MaxArea = fTmp;	
	}
	
	if (pCI->Scope.MinSpeed > pCI->Scope.MaxSpeed)
	{
		iTmp = pCI->Scope.MinSpeed;
		pCI->Scope.MinSpeed = pCI->Scope.MaxSpeed;
		pCI->Scope.MaxSpeed = iTmp;
	}

	char buf[100] = {0,};
	sprintf(buf, "%.1f", pCI->Scope.MinArea);
	pCI->Scope.MinArea = atof(buf);
	sprintf(buf, "%.1f", pCI->Scope.MaxArea);
	pCI->Scope.MaxArea = atof(buf);

	if(pCI->Scope.MinArea < 0)
		pCI->Scope.MinArea = 0;
	if(pCI->Scope.MaxArea > 8) 
		pCI->Scope.MaxArea = 8;
	if(pCI->Scope.MinSpeed < 0)
		pCI->Scope.MinSpeed = 0;
	if(pCI->Scope.MaxSpeed > 70)
		pCI->Scope.MaxSpeed = 70;
}
	
void CClassHelperDlg::OnBnClickedButtonNewclass()
{
	// TODO: Add your control notification handler code here
	ClassInfo CI;
	CString str;
	USES_CONVERSION;

	//Color ID
	CI.ColorID = m_ComboColor.GetCurSel();
	if (CI.ColorID == -1)
	{
		AfxMessageBox(_T("Please choose color!"));
		return;
	}

	//Class Name
	m_EditCtrlClassName.GetWindowText(str);
	if (str.GetLength() == 0)
	{
		AfxMessageBox(_T("Please input Class Name!"));
		return;
	}
	wsprintf(CI.Name, _T("%s"), str);

	//MinArea
	m_EditCtrlMinArea.GetWindowText(str);
	if (str.GetLength() == 0)
	{
		AfxMessageBox(_T("Please input Min Area!"));
		return;
	}
	CI.Scope.MinArea = atof(T2A(str));

	//MaxArea
	m_EditCtrlMaxArea.GetWindowText(str);
	if (str.GetLength() == 0)
	{
		AfxMessageBox(_T("Please input Max Area!"));
		return;
	}
	CI.Scope.MaxArea = atof(T2A(str));

	//MinSpeed
	m_EditCtrlMinSpeed.GetWindowText(str);
	if (str.GetLength() == 0)
	{
		AfxMessageBox(_T("Please input Min Speed!"));
		return;
	}
	CI.Scope.MinSpeed = _wtoi(str);

	//MaxSpeed
	m_EditCtrlMaxSpeed.GetWindowText(str);
	if (str.GetLength() == 0)
	{
		AfxMessageBox(_T("Please input Max Speed!"));
		return;
	}
	CI.Scope.MaxSpeed = _wtoi(str);

	//Hash Value
	CI.ClassID = ID_INVALID;
	if ( m_pCurSelClass == NULL)
		AppendClassToList( m_pHeadClass, &CI );
	else
		UpdateClassToList( m_pCurSelClass, &CI );
	Invalidate();
	UpdateWindow();

}


void CClassHelperDlg::OnKillfocusEditMinArea()
{
	// TODO: Add your control notification handler code here
	if( m_pCurSelClass != NULL)
	{
		CString str;
		USES_CONVERSION;
		//MinArea
		m_EditCtrlMinArea.GetWindowText(str);
		if (str.GetLength() == 0)
		{
			AfxMessageBox(_T("Please input Min Area!"));
			return;
		}
		m_pCurSelClass->Scope.MinArea = atof(T2A(str));
		//Hash Value
		Invalidate();
		UpdateWindow();
	}

}

BOOL CClassHelperDlg::AppendClassToList(ClassInfo* &pHead, ClassInfo *pCI, BOOL bOriClsList)
{
	ClassInfo *pIndex = pHead;

	NormalizationClassInfo(pCI);
	if (pHead == NULL)
	{
		ClassInfo *pHeadCI = new ClassInfo();

		pHeadCI->ClassID = pCI->ClassID;
		if (pHeadCI->ClassID == ID_INVALID)
			pHeadCI->ClassID = AllocClassID();

		if (pHeadCI->ClassID == -1)
		{
			AfxMessageBox(_T("Too many classes!"));
			return FALSE;
		}
		pHeadCI->ColorID = pCI->ColorID;
		wsprintf(pHeadCI->Name, _T("%s"), pCI->Name);
		pHeadCI->Postion = 0;
		pHeadCI->bEnable = pCI->bEnable;

		pHeadCI->Scope.MinArea = pCI->Scope.MinArea;
		pHeadCI->Scope.MaxArea = pCI->Scope.MaxArea;
		pHeadCI->Scope.MinSpeed = pCI->Scope.MinSpeed;
		pHeadCI->Scope.MaxSpeed = pCI->Scope.MaxSpeed;

		pHeadCI->Pre = NULL;
		pHeadCI->Next = NULL;
		pHead = pHeadCI;

		if ( bOriClsList == FALSE)
		{
			m_pCurSelClass = pHeadCI;
			m_TotalClass ++;
			CString str;
			str.Format(_T("%5d         %s"), pHeadCI->ClassID, pHeadCI->Name);
			m_ListClass.InsertString(pHeadCI->Postion, str);
		}

		return TRUE;
	}
	else
	{
		ClassInfo *pCiNode = new ClassInfo();
		pCiNode->ClassID = pCI->ClassID;

		if (pCiNode->ClassID == ID_INVALID)
			pCiNode->ClassID = AllocClassID();

		if (pCiNode->ClassID == -1)
		{
			AfxMessageBox(_T("Too many classes!"));
			return FALSE;
		}
		else if (pCiNode->ClassID == 0)
		{
			pCiNode->ColorID = pCI->ColorID;
			wsprintf(pCiNode->Name, _T("%s"), pCI->Name);
			pCiNode->Postion = 0;
			pCiNode->bEnable = pCI->bEnable;
			pCiNode->Scope.MinArea = pCI->Scope.MinArea;
			pCiNode->Scope.MaxArea = pCI->Scope.MaxArea;
			pCiNode->Scope.MinSpeed = pCI->Scope.MinSpeed;
			pCiNode->Scope.MaxSpeed = pCI->Scope.MaxSpeed;

			pCiNode->Next = m_pHeadClass;
			if(m_pHeadClass != NULL)
				m_pHeadClass->Pre = pCiNode;
			pCiNode->Pre = NULL;
			pHead = pCiNode;

			ClassInfo *p = pCiNode->Next;
			while(p != NULL)
			{
				p->Postion += 1;
				p = p->Next;
			}

			if ( bOriClsList == FALSE)
			{
				m_pCurSelClass = pCiNode;
				m_TotalClass ++;
				CString str;
				str.Format(_T("%5d         %s"), pCiNode->ClassID, pCiNode->Name);
				m_ListClass.InsertString(pCiNode->Postion, str);
			}
			return TRUE;
		}
		else //ClassID != -1 && ClassID != 0
		{
			ClassInfo *pTarget;
			INT32 iDistance = MAX_NUM_CLASS_COUNT - 1;
			pTarget = pHead;
			pIndex = pHead;

			pCiNode->ColorID = pCI->ColorID;
			wsprintf(pCiNode->Name, _T("%s"), pCI->Name);

			while (pIndex != NULL)
			{
				if (pIndex->ClassID + 1 == pCiNode->ClassID)
				{
					pTarget = pIndex;
					break;
				}
				else if( pCiNode->ClassID - pIndex->ClassID > 0 )
				{
					if ( pCiNode->ClassID - pIndex->ClassID < iDistance)
					{
						iDistance = pCiNode->ClassID - pIndex->ClassID;
						pTarget = pIndex;
					}
					pIndex = pIndex->Next;
				}
				else
					pIndex = pIndex->Next;
			}

			pCiNode->Postion = pTarget->Postion + 1;
			pCiNode->bEnable = pCI->bEnable;
			pCiNode->Scope.MinArea = pCI->Scope.MinArea;
			pCiNode->Scope.MaxArea = pCI->Scope.MaxArea;
			pCiNode->Scope.MinSpeed = pCI->Scope.MinSpeed;
			pCiNode->Scope.MaxSpeed = pCI->Scope.MaxSpeed;


			pCiNode->Next = pTarget->Next;
			if(pTarget->Next != NULL)
				pTarget->Next->Pre = pCiNode;
			pCiNode->Pre = pTarget;
			pTarget->Next = pCiNode;

			ClassInfo *p = pCiNode->Next;
			while(p != NULL)
			{
				p->Postion += 1;
				p = p->Next;
			}

			if ( bOriClsList == FALSE)
			{
				m_pCurSelClass = pCiNode;
				m_TotalClass ++;
				CString str;
				str.Format(_T("%5d         %s"), pCiNode->ClassID, pCiNode->Name);
				m_ListClass.InsertString(pCiNode->Postion, str);
			}
			return TRUE;
		}
	}
}

BOOL CClassHelperDlg::UpdateClassToList(ClassInfo *pCurSelClass, ClassInfo *pCI)
{
	if (pCurSelClass != NULL)
	{
		NormalizationClassInfo(pCI);
		pCurSelClass->ColorID = pCI->ColorID;
		wsprintf(pCurSelClass->Name, _T("%s"), pCI->Name);
		m_pCurSelClass->bEnable = m_ComboEnable.GetCurSel();
		pCurSelClass->Scope.MinArea = pCI->Scope.MinArea; 
		pCurSelClass->Scope.MaxArea = pCI->Scope.MaxArea;
		pCurSelClass->Scope.MinSpeed = pCI->Scope.MinSpeed;
		pCurSelClass->Scope.MaxSpeed = pCI->Scope.MaxSpeed;

		CString str;
		str.Format(_T("%5d         %s"), pCurSelClass->ClassID, pCurSelClass->Name);
		m_ListClass.DeleteString(pCurSelClass->Postion);
		m_ListClass.InsertString(pCurSelClass->Postion, str);
		return TRUE;
	}
}

void CClassHelperDlg::DrawAllClass(CPaintDC *pDc)
{
	ClassInfo *pCI = m_pHeadClass;

	CFont *pOldFont, TextFont;

	TextFont.CreateFont(10,
		0,
		0,
		0,
		FW_NORMAL,
		false,
		false,
		false,
		ANSI_CHARSET,
		OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS,
		DEFAULT_QUALITY,
		DEFAULT_PITCH|FF_DONTCARE,
		_T("Arial"));

	pOldFont=pDc->SelectObject(&TextFont);


	while(pCI != NULL)	
	{
		DrawClass(pDc, pCI);
		pCI = pCI->Next;
	}

	if (m_pCurSelClass != NULL)
	{
		CRect ClassScope;
		CPen *pOldPen, DashPen, SolidPen, EndPointPen;

		ClassScope.left = xv2a(m_pCurSelClass->Scope.MinArea);
		ClassScope.right = xv2a(m_pCurSelClass->Scope.MaxArea);
		ClassScope.bottom = yv2a(m_pCurSelClass->Scope.MinSpeed);
		ClassScope.top = yv2a(m_pCurSelClass->Scope.MaxSpeed);

		CBrush ClassColor(m_ColorLUT[m_pCurSelClass->ColorID]);
		pDc->FillRect(&ClassScope, &ClassColor);

		DashPen.CreatePen(PS_DOT,1,RGB(0, 0, 0));
		SolidPen.CreatePen(PS_SOLID,1,RGB(0, 0, 0));
		EndPointPen.CreatePen(PS_SOLID, 2, m_ColorLUT[m_pCurSelClass->ColorID]);

//		pOldPen=pDc->SelectObject(&SolidPen);
//		pDc->MoveTo(ClassScope.left, ClassScope.top);
//		pDc->LineTo(ClassScope.right, ClassScope.top);
//		pDc->LineTo(ClassScope.right, ClassScope.bottom);
//		pDc->LineTo(ClassScope.left, ClassScope.bottom);
//		pDc->LineTo(ClassScope.left, ClassScope.top);

		pOldPen = pDc->SelectObject(&DashPen);
		pDc->MoveTo(ClassScope.left, ClassScope.top);
		pDc->LineTo(ClassScope.left, yv2a(0));
		pDc->MoveTo(ClassScope.right, ClassScope.top);
		pDc->LineTo(ClassScope.right, yv2a(0));
		pDc->MoveTo(ClassScope.right, ClassScope.top);
		pDc->LineTo(xv2a(0), ClassScope.top);
		pDc->MoveTo(ClassScope.right, ClassScope.bottom);
		pDc->LineTo(xv2a(0), ClassScope.bottom);


		CString str;
		str.Format(_T("%.1f"), m_pCurSelClass->Scope.MinArea);
		pDc->TextOut(ClassScope.left - 5, yv2a(-1), str);
		str.Format(_T("%.1f"), m_pCurSelClass->Scope.MaxArea);
		pDc->TextOut(ClassScope.right - 5, yv2a(-1), str);
		str.Format(_T("%d"), m_pCurSelClass->Scope.MinSpeed);
		pDc->TextOut(xv2a(-0.2), ClassScope.bottom - 3, str);
		str.Format(_T("%d"), m_pCurSelClass->Scope.MaxSpeed);
		pDc->TextOut(xv2a(-0.2), ClassScope.top - 3, str);

		pDc->TextOut(ClassScope.left + 2, (ClassScope.top + ClassScope.bottom)/2 - 3, m_pCurSelClass->Name);

		pDc->SelectObject(&EndPointPen);
		pDc->Ellipse(ClassScope.left - 5, ClassScope.top - 5, ClassScope.left + 5, ClassScope.top + 5);
		pDc->Ellipse(ClassScope.left - 5, ClassScope.bottom - 5, ClassScope.left + 5, ClassScope.bottom + 5);
		pDc->Ellipse(ClassScope.right - 5, ClassScope.bottom - 5, ClassScope.right + 5, ClassScope.bottom + 5);
		pDc->Ellipse(ClassScope.right - 5, ClassScope.top - 5, ClassScope.right + 5, ClassScope.top + 5);

		pDc->SelectObject(pOldPen);
		pDc->SelectObject(pOldFont);
		TextFont.DeleteObject();
		DashPen.DeleteObject();
	}

}


void CClassHelperDlg::DrawClassCount(CPaintDC *pDc)
{
	CFont *pOldFont, TextFont, CountFont;

	TextFont.CreateFont(18,
					0,
					0,
					0,
					FW_NORMAL,
					false,
					false,
					false,
					ANSI_CHARSET,
					OUT_DEFAULT_PRECIS,
					CLIP_DEFAULT_PRECIS,
					DEFAULT_QUALITY,
					DEFAULT_PITCH|FF_DONTCARE,
					_T("Arial"));

	CString Count;
	Count.Format(_T("%d"), m_TotalClass);
	pOldFont=pDc->SelectObject(&TextFont);
	pDc->TextOut(xv2a(9.2), yv2a(0), _T("Total Classes��"));
	pDc->TextOut(xv2a(11.2), yv2a(0), Count);

	pDc->SelectObject(pOldFont);
	TextFont.DeleteObject();
	CountFont.DeleteObject();
}


void CClassHelperDlg::OnLButtonDown(UINT nFlags, CPoint Point)
{
	// TODO: Add your message handler code here and/or call default
	if (Point.x > 540 || Point.y > m_OriginPoint.y)
		return;

	if (m_pCurSelClass != NULL)
	{
		//Class Name
		CString str;
		USES_CONVERSION;

		m_EditCtrlClassName.GetWindowText(str);
		if (str.GetLength() != 0)
			wsprintf(m_pCurSelClass->Name, _T("%s"), str);
		str.Format(_T("%5d         %s"), m_pCurSelClass->ClassID, m_pCurSelClass->Name);
		m_ListClass.DeleteString(m_pCurSelClass->Postion);
		m_ListClass.InsertString(m_pCurSelClass->Postion, str);
		m_pCurSelClass->bEnable = m_ComboEnable.GetCurSel();

		//MinArea
		m_EditCtrlMinArea.GetWindowText(str);
		if (str.GetLength() != 0)
			m_pCurSelClass->Scope.MinArea = atof(T2A(str));
		//MaxArea
		m_EditCtrlMaxArea.GetWindowText(str);
		if (str.GetLength() != 0)
			m_pCurSelClass->Scope.MaxArea = atof(T2A(str));
		//MinSpeed
		m_EditCtrlMinSpeed.GetWindowText(str);
		if (str.GetLength() != 0)
			m_pCurSelClass->Scope.MinSpeed = _wtoi(str);
		//MaxSpeed
		m_EditCtrlMaxSpeed.GetWindowText(str);
		if (str.GetLength() != 0)
			m_pCurSelClass->Scope.MaxSpeed = _wtoi(str);
	}
	FindSelClass(Point);
	Invalidate();
	UpdateWindow();
	CDialogEx::OnLButtonDown(nFlags, Point);
}

BOOL CClassHelperDlg::FindSelClass(CPoint Point)
{
	CRect rect;
	m_SelClaseBase.SelPos = Pos_NONE;

	if (Point.x > 540 || Point.y > m_OriginPoint.y)
	{
		return false;
	}

	if ( m_pCurSelClass != NULL )
	{
		rect.top = yv2a(m_pCurSelClass->Scope.MaxSpeed);
		rect.bottom = yv2a(m_pCurSelClass->Scope.MinSpeed);
		rect.left = xv2a(m_pCurSelClass->Scope.MinArea);
		rect.right = xv2a(m_pCurSelClass->Scope.MaxArea);
		rect.NormalizeRect();

		if(5 >= sqrtl(((Point.x - rect.left)*(Point.x - rect.left) + (Point.y - rect.top)*(Point.y - rect.top))))
		{
			m_SelClaseBase.SelPos = Pos_LeftTop;
			m_SelClaseBase.BaseScope.MinArea = m_pCurSelClass->Scope.MinArea;
			m_SelClaseBase.BaseScope.MaxArea = m_pCurSelClass->Scope.MaxArea;
			m_SelClaseBase.BaseScope.MinSpeed = m_pCurSelClass->Scope.MinSpeed;
			m_SelClaseBase.BaseScope.MaxSpeed = m_pCurSelClass->Scope.MaxSpeed;
			m_SelClaseBase.BasePoint.x = Point.x;
			m_SelClaseBase.BasePoint.y = Point.y;
			return true;
		}
		else if(5 >= sqrtl(((Point.x - rect.left)*(Point.x - rect.left) + (Point.y - rect.bottom)*(Point.y - rect.bottom))))
		{
			m_SelClaseBase.SelPos = Pos_LeftBottom;
			m_SelClaseBase.BaseScope.MinArea = m_pCurSelClass->Scope.MinArea;
			m_SelClaseBase.BaseScope.MaxArea = m_pCurSelClass->Scope.MaxArea;
			m_SelClaseBase.BaseScope.MinSpeed = m_pCurSelClass->Scope.MinSpeed;
			m_SelClaseBase.BaseScope.MaxSpeed = m_pCurSelClass->Scope.MaxSpeed;
			m_SelClaseBase.BasePoint.x = Point.x;
			m_SelClaseBase.BasePoint.y = Point.y;
			return true;
		}
		else if(5 >= sqrtl(((Point.x - rect.right)*(Point.x - rect.right) + (Point.y - rect.bottom)*(Point.y - rect.bottom))))
		{
			m_SelClaseBase.SelPos = Pos_RightBottom;
			m_SelClaseBase.BaseScope.MinArea = m_pCurSelClass->Scope.MinArea;
			m_SelClaseBase.BaseScope.MaxArea = m_pCurSelClass->Scope.MaxArea;
			m_SelClaseBase.BaseScope.MinSpeed = m_pCurSelClass->Scope.MinSpeed;
			m_SelClaseBase.BaseScope.MaxSpeed = m_pCurSelClass->Scope.MaxSpeed;
			m_SelClaseBase.BasePoint.x = Point.x;
			m_SelClaseBase.BasePoint.y = Point.y;
			return true;
		}
		else if(5 >= sqrtl(((Point.x - rect.right)*(Point.x - rect.right) + (Point.y - rect.top)*(Point.y - rect.top))))
		{
			m_SelClaseBase.SelPos = Pos_RightTop;
			m_SelClaseBase.BaseScope.MinArea = m_pCurSelClass->Scope.MinArea;
			m_SelClaseBase.BaseScope.MaxArea = m_pCurSelClass->Scope.MaxArea;
			m_SelClaseBase.BaseScope.MinSpeed = m_pCurSelClass->Scope.MinSpeed;
			m_SelClaseBase.BaseScope.MaxSpeed = m_pCurSelClass->Scope.MaxSpeed;
			m_SelClaseBase.BasePoint.x = Point.x;
			m_SelClaseBase.BasePoint.y = Point.y;

			return true;
		}
		else if (rect.PtInRect(Point))
		{

			m_SelClaseBase.SelPos = Pos_IN;
			m_SelClaseBase.BaseScope.MinArea = m_pCurSelClass->Scope.MinArea;
			m_SelClaseBase.BaseScope.MaxArea = m_pCurSelClass->Scope.MaxArea;
			m_SelClaseBase.BaseScope.MinSpeed = m_pCurSelClass->Scope.MinSpeed;
			m_SelClaseBase.BaseScope.MaxSpeed = m_pCurSelClass->Scope.MaxSpeed;
			m_SelClaseBase.BasePoint.x = Point.x;
			m_SelClaseBase.BasePoint.y = Point.y;
			return true;
		}
		else
			m_pCurSelClass = NULL;
	}

	//m_pCurSelClass == NULL
	ClassInfo *pCI = m_pHeadClass;
	while(pCI != NULL)
	{	
		rect.top = yv2a(pCI->Scope.MaxSpeed);
		rect.bottom = yv2a(pCI->Scope.MinSpeed);
		rect.left = xv2a(pCI->Scope.MinArea);
		rect.right = xv2a(pCI->Scope.MaxArea);
		rect.NormalizeRect();

		if(5 >= sqrtl(((Point.x - rect.left)*(Point.x - rect.left) + (Point.y - rect.top)*(Point.y - rect.top))))
		{
			m_pCurSelClass = pCI;
			m_SelClaseBase.SelPos = Pos_LeftTop;
			m_SelClaseBase.BaseScope.MinArea = m_pCurSelClass->Scope.MinArea;
			m_SelClaseBase.BaseScope.MaxArea = m_pCurSelClass->Scope.MaxArea;
			m_SelClaseBase.BaseScope.MinSpeed = m_pCurSelClass->Scope.MinSpeed;
			m_SelClaseBase.BaseScope.MaxSpeed = m_pCurSelClass->Scope.MaxSpeed;
			m_SelClaseBase.BasePoint.x = Point.x;
			m_SelClaseBase.BasePoint.y = Point.y;
			return true;
		}
		else if(5 >= sqrtl(((Point.x - rect.left)*(Point.x - rect.left) + (Point.y - rect.bottom)*(Point.y - rect.bottom))))
		{
			m_pCurSelClass = pCI;
			m_SelClaseBase.SelPos = Pos_LeftBottom;
			m_SelClaseBase.BaseScope.MinArea = m_pCurSelClass->Scope.MinArea;
			m_SelClaseBase.BaseScope.MaxArea = m_pCurSelClass->Scope.MaxArea;
			m_SelClaseBase.BaseScope.MinSpeed = m_pCurSelClass->Scope.MinSpeed;
			m_SelClaseBase.BaseScope.MaxSpeed = m_pCurSelClass->Scope.MaxSpeed;
			m_SelClaseBase.BasePoint.x = Point.x;
			m_SelClaseBase.BasePoint.y = Point.y;
			return true;
		}
		else if(5 >= sqrtl(((Point.x - rect.right)*(Point.x - rect.right) + (Point.y - rect.bottom)*(Point.y - rect.bottom))))
		{
			m_pCurSelClass = pCI;
			m_SelClaseBase.SelPos = Pos_RightBottom;
			m_SelClaseBase.BaseScope.MinArea = m_pCurSelClass->Scope.MinArea;
			m_SelClaseBase.BaseScope.MaxArea = m_pCurSelClass->Scope.MaxArea;
			m_SelClaseBase.BaseScope.MinSpeed = m_pCurSelClass->Scope.MinSpeed;
			m_SelClaseBase.BaseScope.MaxSpeed = m_pCurSelClass->Scope.MaxSpeed;
			m_SelClaseBase.BasePoint.x = Point.x;
			m_SelClaseBase.BasePoint.y = Point.y;
			return true;
		}
		else if(5 >= sqrtl(((Point.x - rect.right)*(Point.x - rect.right) + (Point.y - rect.top)*(Point.y - rect.top))))
		{
			m_pCurSelClass = pCI;
			m_SelClaseBase.SelPos = Pos_RightTop;
			m_SelClaseBase.BaseScope.MinArea = m_pCurSelClass->Scope.MinArea;
			m_SelClaseBase.BaseScope.MaxArea = m_pCurSelClass->Scope.MaxArea;
			m_SelClaseBase.BaseScope.MinSpeed = m_pCurSelClass->Scope.MinSpeed;
			m_SelClaseBase.BaseScope.MaxSpeed = m_pCurSelClass->Scope.MaxSpeed;
			m_SelClaseBase.BasePoint.x = Point.x;
			m_SelClaseBase.BasePoint.y = Point.y;
			return true;
		}
		else if (rect.PtInRect(Point))
		{
			m_pCurSelClass = pCI;
			m_SelClaseBase.SelPos = Pos_IN;
			m_SelClaseBase.BaseScope.MinArea = m_pCurSelClass->Scope.MinArea;
			m_SelClaseBase.BaseScope.MaxArea = m_pCurSelClass->Scope.MaxArea;
			m_SelClaseBase.BaseScope.MinSpeed = m_pCurSelClass->Scope.MinSpeed;
			m_SelClaseBase.BaseScope.MaxSpeed = m_pCurSelClass->Scope.MaxSpeed;
			m_SelClaseBase.BasePoint.x = Point.x;
			m_SelClaseBase.BasePoint.y = Point.y;
			return true;
		}
		pCI = pCI->Next;
	}
	return false;
}

void CClassHelperDlg::DeleteSelClassFromList()
{
	if (m_pCurSelClass != NULL)
	{
		ClassInfo *p = m_pCurSelClass->Next;

		while(p != NULL)
		{
			p->Postion -= 1;
			p = p->Next;
		}
		m_ListClass.DeleteString(m_pCurSelClass->Postion);

		if (m_pCurSelClass->Pre != NULL)
			m_pCurSelClass->Pre->Next = m_pCurSelClass->Next;
		else
			m_pHeadClass = m_pCurSelClass->Next;

		if (m_pCurSelClass->Next != NULL)
			m_pCurSelClass->Next->Pre = m_pCurSelClass->Pre;

		delete m_pCurSelClass;
		m_pCurSelClass = NULL;
		//update Total class count
		m_TotalClass --;
	}
}

void CClassHelperDlg::OnClickedButtonDeleteClass()
{
	// TODO: Add your control notification handler code here
	DeleteSelClassFromList();
	Invalidate();
	UpdateWindow();
}


void CClassHelperDlg::OnClickedButtonDelallclass()
{
	// TODO: Add your control notification handler code here
	ClassInfo *pCI = m_pHeadClass;
	while(pCI != NULL)
	{
		if (pCI->Next != NULL)
		{
			pCI = pCI->Next;
			delete pCI->Pre;
		}
		else
		{
			delete pCI;
			pCI = NULL;
		}
	}
		
	//Init all var
	m_pCurSelClass = NULL;
	m_pHeadClass = NULL;
	m_TotalClass = 0;
	m_CurSelClassID = -1;
	m_ListClass.ResetContent();
	Invalidate();
	UpdateWindow();
}

void CClassHelperDlg::UpdateCtrlStatus(void)
{
	if (m_pCurSelClass == NULL)
	{
		m_BtnNewClass.SetWindowText(_T("New Class"));
		m_CurSelClassID = 0;
		m_EditCtrlClassName.SetWindowText(_T(""));
		m_ListClass.SetCurSel(-1);
		m_ComboColor.SetCurSel(-1);
		m_ComboEnable.SetCurSel(1);
		m_EditCtrlMinArea.SetWindowText(_T(""));
		m_EditCtrlMaxArea.SetWindowText(_T(""));
		m_EditCtrlMinSpeed.SetWindowText(_T(""));
		m_EditCtrlMaxSpeed.SetWindowText(_T(""));
		m_BtnMoveDown.EnableWindow(FALSE);
		m_BtnMoveUp.EnableWindow(FALSE);
	}
	else
	{

		CString str;

		m_BtnNewClass.SetWindowText(_T("Update Class"));
		m_CurSelClassID = m_pCurSelClass->ClassID;
		m_ComboColor.SetCurSel(m_pCurSelClass->ColorID);
		m_EditCtrlClassName.SetWindowText(m_pCurSelClass->Name);
		m_ComboEnable.SetCurSel(m_pCurSelClass->bEnable);

		str.Format(_T("%.1f"), m_pCurSelClass->Scope.MinArea);
		m_EditCtrlMinArea.SetWindowText(str);

		str.Format(_T("%.1f"), m_pCurSelClass->Scope.MaxArea);
		m_EditCtrlMaxArea.SetWindowText(str);

		str.Format(_T("%d"), m_pCurSelClass->Scope.MinSpeed);
		m_EditCtrlMinSpeed.SetWindowText(str);

		str.Format(_T("%d"), m_pCurSelClass->Scope.MaxSpeed);
		m_EditCtrlMaxSpeed.SetWindowText(str);

		m_ListClass.SetCurSel(m_pCurSelClass->Postion);
		if (m_ListClass.GetCurSel() == 0)
		{
			m_BtnMoveDown.EnableWindow(TRUE);
			m_BtnMoveUp.EnableWindow(FALSE);
		}
		else if ( m_ListClass.GetCurSel() == m_ListClass.GetCount() - 1 )
		{
			m_BtnMoveDown.EnableWindow(FALSE);
			m_BtnMoveUp.EnableWindow(TRUE);
		}
		else
		{
			m_BtnMoveDown.EnableWindow(TRUE);
			m_BtnMoveUp.EnableWindow(TRUE);
		}

	}
}

void CClassHelperDlg::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	if (FindSelClass(point))
		return;
	if (point.x > 540 || point.y > m_OriginPoint.y || point.x < m_OriginPoint.x || point.y < 70)
		return ;

	ClassInfo CI;
	CI.ClassID = ID_INVALID;
	CI.ColorID = m_TotalClass % 7;
	wsprintf(CI.Name, _T("%s"), _T("Name"));
	CI.bEnable = m_ComboEnable.GetCurSel();
	CI.Scope.MinArea = xa2v(point.x) - 0.5;
	CI.Scope.MaxArea = xa2v(point.x) + 0.5;
	CI.Scope.MinSpeed = ya2v(point.y) - 5;
	CI.Scope.MaxSpeed = ya2v(point.y) + 5;
	CI.Pre = NULL;
	CI.Next = NULL;
	AppendClassToList(m_pHeadClass, &CI);
	Invalidate();
	UpdateWindow();

	CDialogEx::OnLButtonDblClk(nFlags, point);
}


void CClassHelperDlg::OnMouseMove(UINT nFlags, CPoint Point)
{
	// TODO: Add your message handler code here and/or call default
	if(nFlags & MK_LBUTTON && m_pCurSelClass != NULL && m_SelClaseBase.SelPos != Pos_NONE)
	{
		float NewX;
		INT32 NewY;

		NewX = xa2v(Point.x) - xa2v(m_SelClaseBase.BasePoint.x);
		NewY = ya2v(Point.y) - ya2v(m_SelClaseBase.BasePoint.y); 
		switch (m_SelClaseBase.SelPos)
		{
		case Pos_LeftTop:
			{
				if( m_SelClaseBase.BaseScope.MinArea + NewX >= 0 &&
					m_SelClaseBase.BaseScope.MinArea + NewX + 0.1 <= m_SelClaseBase.BaseScope.MaxArea &&
					m_SelClaseBase.BaseScope.MaxSpeed + NewY > m_SelClaseBase.BaseScope.MinSpeed &&
					m_SelClaseBase.BaseScope.MaxSpeed + NewX <= 70)
				{
					m_pCurSelClass->Scope.MinArea = m_SelClaseBase.BaseScope.MinArea + NewX;
					m_pCurSelClass->Scope.MaxSpeed = m_SelClaseBase.BaseScope.MaxSpeed + NewY;
					NormalizationClassInfo(m_pCurSelClass);
					Invalidate();
					UpdateWindow();
				}
				break;
			}
		case Pos_LeftBottom:
			{
				if( m_SelClaseBase.BaseScope.MinArea + NewX >= 0 &&
					m_SelClaseBase.BaseScope.MinArea + NewX + 0.1 <= m_SelClaseBase.BaseScope.MaxArea &&
					m_SelClaseBase.BaseScope.MinSpeed + NewY < m_SelClaseBase.BaseScope.MaxSpeed &&
					m_SelClaseBase.BaseScope.MinSpeed + NewX >= 0)
				{
					m_pCurSelClass->Scope.MinArea = m_SelClaseBase.BaseScope.MinArea + NewX;
					m_pCurSelClass->Scope.MinSpeed = m_SelClaseBase.BaseScope.MinSpeed + NewY;
					NormalizationClassInfo(m_pCurSelClass);
					Invalidate();
					UpdateWindow();
				}
				break;
			}
		case Pos_RightBottom:
			{
				if( m_SelClaseBase.BaseScope.MaxArea + NewX <= 8 &&
					m_SelClaseBase.BaseScope.MaxArea + NewX - 0.1 >= m_SelClaseBase.BaseScope.MinArea &&
					m_SelClaseBase.BaseScope.MinSpeed + NewY < m_SelClaseBase.BaseScope.MaxSpeed &&
					m_SelClaseBase.BaseScope.MinSpeed + NewX >= 0)
				{
					m_pCurSelClass->Scope.MaxArea = m_SelClaseBase.BaseScope.MaxArea + NewX;
					m_pCurSelClass->Scope.MinSpeed = m_SelClaseBase.BaseScope.MinSpeed + NewY;
					NormalizationClassInfo(m_pCurSelClass);
					Invalidate();
					UpdateWindow();
				}
				break;
			}
		case Pos_RightTop:
			{
				if( m_SelClaseBase.BaseScope.MaxArea + NewX <= 8 &&
					m_SelClaseBase.BaseScope.MaxArea + NewX - 0.1 >= m_SelClaseBase.BaseScope.MinArea &&
					m_SelClaseBase.BaseScope.MaxSpeed + NewY > m_SelClaseBase.BaseScope.MinSpeed &&
					m_SelClaseBase.BaseScope.MaxSpeed + NewX <= 70)
				{
					m_pCurSelClass->Scope.MaxArea = m_SelClaseBase.BaseScope.MaxArea + NewX;
					m_pCurSelClass->Scope.MaxSpeed = m_SelClaseBase.BaseScope.MaxSpeed + NewY;
					NormalizationClassInfo(m_pCurSelClass);
					Invalidate();
					UpdateWindow();
				}
				break;
			}
		case Pos_IN:
			{
				if( m_SelClaseBase.BaseScope.MinArea + NewX >= 0 && 
					m_SelClaseBase.BaseScope.MinSpeed + NewY >= 0 &&
					m_SelClaseBase.BaseScope.MaxArea + NewX <= 8 &&
					m_SelClaseBase.BaseScope.MaxSpeed + NewY <= 70)
				{
					m_pCurSelClass->Scope.MinArea = m_SelClaseBase.BaseScope.MinArea + NewX;
					m_pCurSelClass->Scope.MaxArea = m_SelClaseBase.BaseScope.MaxArea + NewX;
					m_pCurSelClass->Scope.MinSpeed = m_SelClaseBase.BaseScope.MinSpeed + NewY;
					m_pCurSelClass->Scope.MaxSpeed = m_SelClaseBase.BaseScope.MaxSpeed + NewY;
					NormalizationClassInfo(m_pCurSelClass);
					Invalidate();
					UpdateWindow();
				}
				break;
			}
		}

	}
	
	CDialogEx::OnMouseMove(nFlags, Point);
}


void CClassHelperDlg::OnSelchangeComboColor()
{
	// TODO: Add your control notification handler code here
	//Color ID
	if(m_pCurSelClass != NULL)
	{
		m_pCurSelClass->ColorID = m_ComboColor.GetCurSel();
		if (m_pCurSelClass->ColorID == -1)
		{
			AfxMessageBox(_T("Please choose color!"));
			return;
		}
		Invalidate();
		UpdateWindow();
	}

}


//void CClassHelperDlg::OnChangeEditMinArea()
//{
//	// TODO:  If this is a RICHEDIT control, the control will not
//	// send this notification unless you override the CDialogEx::OnInitDialog()
//	// function and call CRichEditCtrl().SetEventMask()
//	// with the ENM_CHANGE flag ORed into the mask.
//
//	// TODO:  Add your control notification handler code here
//
//}


void CClassHelperDlg::OnKillfocusEditMaxArea()
{
	// TODO: Add your control notification handler code here
	if( m_pCurSelClass != NULL)
	{
		CString str;
		USES_CONVERSION;
		//MaxArea
		m_EditCtrlMaxArea.GetWindowText(str);
		if (str.GetLength() == 0)
		{
			AfxMessageBox(_T("Please input Max Area!"));
			return;
		}
		m_pCurSelClass->Scope.MaxArea = atof(T2A(str));
		Invalidate();
		UpdateWindow();
	}
}


void CClassHelperDlg::OnKillfocusEditMinSpeed()
{
	// TODO: Add your control notification handler code here
	if( m_pCurSelClass != NULL)
	{
		CString str;
		USES_CONVERSION;
		//MinSpeed
		m_EditCtrlMinSpeed.GetWindowText(str);
		if (str.GetLength() == 0)
		{
			AfxMessageBox(_T("Please input Min Speed!"));
			return;
		}
		m_pCurSelClass->Scope.MinSpeed = _wtoi(str);
		Invalidate();
		UpdateWindow();
	}

}


void CClassHelperDlg::OnKillfocusEditMaxSpeed()
{
	// TODO: Add your control notification handler code here
	if( m_pCurSelClass != NULL)
	{
		CString str;
		USES_CONVERSION;
		//MinSpeed
		m_EditCtrlMaxSpeed.GetWindowText(str);
		if (str.GetLength() == 0)
		{
			AfxMessageBox(_T("Please input Min Speed!"));
			return;
		}
		m_pCurSelClass->Scope.MaxSpeed = _wtoi(str);
		Invalidate();
		UpdateWindow();
	}
}


void CClassHelperDlg::OnKillfocusEditClassname()
{
	// TODO: Add your control notification handler code here
	if( m_pCurSelClass != NULL)
	{
		CString str;
		//Class Name
		m_EditCtrlClassName.GetWindowText(str);
		if (str.GetLength() == 0)
		{
			AfxMessageBox(_T("Please input Class Name!"));
			return;
		}
		wsprintf(m_pCurSelClass->Name, _T("%s"), str);

		CString szStr;
		szStr.Format(_T("%5d         %s"), m_pCurSelClass->ClassID, m_pCurSelClass->Name);
		m_ListClass.DeleteString(m_pCurSelClass->Postion);
		m_ListClass.InsertString(m_pCurSelClass->Postion, szStr);

		Invalidate();
		UpdateWindow();
	}
}


INT32 CClassHelperDlg::AllocClassID()
{
	INT32 i;
	BOOL bIdUsed;
	ClassInfo *pCI;

	for(i = 0; i < MAX_NUM_CLASS_COUNT; i ++)
	{
		bIdUsed = FALSE;
		pCI = m_pHeadClass;

		if(pCI == NULL)
			return 0;

		while (pCI != NULL)
		{
			if (pCI->ClassID == i )				
			{
				bIdUsed = TRUE;
				break;
			}
			else
				pCI = pCI->Next;
		}

		if (bIdUsed == FALSE)
			return i;
	}
	return -1;
}

void CClassHelperDlg::OnSelchangeListClass()
{
	// TODO: Add your control notification handler code here
	INT32 CurPos = m_ListClass.GetCurSel();
	m_pCurSelClass = m_pHeadClass;

	while (m_pCurSelClass != NULL)
	{
		if (m_pCurSelClass->Postion == CurPos)
			break;
		else
			m_pCurSelClass = m_pCurSelClass->Next;
	}
	Invalidate();
	UpdateWindow();
}

void CClassHelperDlg::ClearClassList(ClassInfo* &pHead)
{
	ClassInfo *pCI = pHead;
	while(pCI != NULL)
	{
		if (pCI->Next != NULL)
		{
			pCI = pCI->Next;
			delete pCI->Pre;
		}
		else
		{
			delete pCI;
			pCI = NULL;
		}
	}

	pHead = NULL;
}

void CClassHelperDlg::OnClickedBtnDownload()
{
	// TODO: Add your control notification handler code here
	BYTE ipaddr[4] = {0,};
	m_IPaddr.GetAddress(ipaddr[0], ipaddr[1], ipaddr[2], ipaddr[3]);
	m_WebServerIP.Format(_T("%d.%d.%d.%d"), ipaddr[0], ipaddr[1], ipaddr[2], ipaddr[3]);
//	AfxMessageBox(WebServerIP);
	
	((CEdit *)GetDlgItem(IDC_EDIT_USERNAME))->GetWindowText(m_Username);
	((CEdit *)GetDlgItem(IDC_EDIT_PASSWORD))->GetWindowText(m_Password);

	CString szPort;
	((CEdit *)GetDlgItem(IDC_EDIT_PORT))->GetWindowText(szPort);
	m_Port = _wtoi(szPort);
	
	if (BST_CHECKED == ((CButton*)GetDlgItem(IDC_CHECK_SSL))->GetCheck())
		m_bSSL = TRUE;
	else
		m_bSSL = FALSE;

	ClearClassList(m_pOriClassList);
	ClearClassList(m_pHeadClass);
	m_pCurSelClass = NULL;
	m_TotalClass = 0;
	m_CurSelClassID = -1;
	m_ListClass.ResetContent();

	DWORD dwresponse = 256 * 1024;
	BYTE* pcbresponse;

	pcbresponse = new BYTE[dwresponse];
	ZeroMemory( pcbresponse, dwresponse );
	_request_cgicommand(_VCA_CGI_, _T("action=list&group=VCA.ch0.cg0"), pcbresponse, &dwresponse );
	// Read all to linked list

	struct tLL	tLLTop;
	memset( &tLLTop, 0, sizeof(tLLTop) );
	tLLTop.ptend = &tLLTop;
	parsing_readbuffer2linkedlist( (char *)pcbresponse, (unsigned char*)"=", &tLLTop );
	RetrieveClassInfro(tLLTop);
	Invalidate();
	UpdateWindow();	
}


BOOL CClassHelperDlg::RetrieveClassInfro(struct tLL &tLLTop)
{
	struct tLL *ptLL = NULL;
	char szgrp[128] = {0,};
	INT32 i;
	BOOL fRet = NULL;
	INT32 iChanelNo = 0;
	INT32 iClassGroup = 0;
	USES_CONVERSION;

	//Read all object classifier info
	for( i = 0; i < MAX_NUM_CLASS_COUNT; i ++ )
	{
		ClassInfo CI;
		memset(&CI, 0, sizeof(ClassInfo));

		CI.ClassID = i;
		CI.ColorID = m_TotalClass % 7;
		//Name
		sprintf( szgrp, "VCA.Ch%d.Cg%d.Oc%d.name", iChanelNo, iClassGroup, i );
		ptLL = parsing_linkedlist_findleft(&tLLTop, szgrp);
		if (ptLL)
		{
			lstrcpy(CI.Name, A2T(ptLL->pszright));
		}
		else
		{
			CI.ClassID = ID_INVALID;
			CI.bEnable = 0;
			fRet = FALSE;
			continue;
		}

		//enable
		sprintf( szgrp, "VCA.Ch%d.Cg%d.Oc%d.enable", iChanelNo, iClassGroup, i );
		ptLL = parsing_linkedlist_findleft( &tLLTop, szgrp );
		if ( ptLL )
		{
			if ( pcgi_ismatch( ptLL->pszright, "yes" ) )
				CI.bEnable = 1;
			else 
				CI.bEnable = 0;
		}

		// area
		sprintf( szgrp, "VCA.Ch%d.Cg%d.Oc%d.area", iChanelNo, iClassGroup, i );
		ptLL = parsing_linkedlist_findleft( &tLLTop, szgrp );
		if ( ptLL )
		{
			int		ncount = MAX_NUM_AREAFILTERS;
			char	aszvalue[MAX_NUM_AREAFILTERS][128];
			if( 0 == parsing_gettokenvalue(ptLL ->pszright, ",\r\n", aszvalue, &ncount ) ) 
			{
				for ( int j = 0; j < 1; j++ )
				{
					int		ncnt = 2;
					char	szval[2][128];
					if( 0 == parsing_gettokenvalue( aszvalue[j], ":\r\n", szval, &ncnt ) ) 
					{
						CI.Scope.MinArea = atof( szval[0] )/10;
						CI.Scope.MaxArea = atof( szval[1] )/10;
					}
				}
			}
		}	

		// speed
		sprintf( szgrp, "VCA.Ch%d.Cg%d.Oc%d.speed", iChanelNo, iClassGroup, i );
		ptLL = parsing_linkedlist_findleft( &tLLTop, szgrp );
		if ( ptLL )
		{
			int		ncount = MAX_NUM_AREAFILTERS;
			char	aszvalue[MAX_NUM_AREAFILTERS][128];
			if( 0 == parsing_gettokenvalue(ptLL ->pszright, ",\r\n", aszvalue, &ncount ) ) 
			{
				for ( int j = 0; j < 1; j++ )
				{
					int		ncnt = 2;
					char	szval[2][128];
					if( 0 == parsing_gettokenvalue( aszvalue[j], ":\r\n", szval, &ncnt ) ) 
					{
						CI.Scope.MinSpeed = atof( szval[0] );
						CI.Scope.MaxSpeed = atof( szval[1] );
					}
				}
			}
		}
		AppendClassToList(m_pOriClassList, &CI, TRUE);
		AppendClassToList(m_pHeadClass, &CI);
		m_pCurSelClass = NULL;
	}
	return TRUE;
}

BOOL CClassHelperDlg::pcgi_ismatch( char* _psz1, char* _psz2 )
{
	int	nret;
	nret = FALSE;

	if( 0 == strcmp( _psz1, _psz2 ) ) nret = TRUE;
	// return value of boolean type
	return nret;
}


void CClassHelperDlg::OnClickedBtnUpload()
{
	// TODO: Add your control notification handler code here
	ClassInfo *pOriCI = NULL;
	ClassInfo *pCI = NULL;
	INT32 iHandledCls = 0;
	DWORD dwresponse = 256 * 1024;
	BYTE* pcbresponse;
	BOOL fRet = 0;
	CString str;

	pcbresponse = new BYTE[dwresponse];
	ZeroMemory( pcbresponse, dwresponse );

	for (INT32 i = 0; i < MAX_NUM_CLASS_COUNT; i++)
	{
		ZeroMemory( pcbresponse, dwresponse );
		pOriCI = FindClassID(m_pOriClassList, i);
		pCI = FindClassID(m_pHeadClass, i);
		if (pOriCI != NULL && pCI != NULL)
		{
			if (!lstrcmp(pOriCI->Name, pCI->Name) &&
				pOriCI->bEnable == pCI->bEnable &&
				pOriCI->Scope.MinArea == pCI->Scope.MinArea &&
				pOriCI->Scope.MaxArea == pCI->Scope.MaxArea &&
				pOriCI->Scope.MinSpeed == pCI->Scope.MinSpeed &&
				pOriCI->Scope.MaxSpeed == pCI->Scope.MaxSpeed)
			{
				iHandledCls ++;
				continue;
			}
			else
			{
				str.Format(_T("action=update&group=VCA.ch0.cg0.oc%d&name=%s&enable=%s&area=%f:%f&speed=%d:%d"),
					pCI->ClassID, pCI->Name, pCI->bEnable == 1? _T("yes"):_T("no"), 10 * pCI->Scope.MinArea, 10 * pCI->Scope.MaxArea, pCI->Scope.MinSpeed, pCI->Scope.MaxSpeed );
				fRet = _request_cgicommand(_VCA_CGI_, str.GetBuffer(), pcbresponse, &dwresponse );
				str.ReleaseBuffer();
				if (fRet == FALSE)
					AfxMessageBox(_T("_request_cgicomand failed!"));
				iHandledCls ++;
			}
		}
		else if (pOriCI == NULL && pCI != NULL)
		{
			str.Format(_T("action=add&group=VCA.ch0.cg0&object=objcls&name=%s&enable=%s&area=%f:%f&speed=%d:%d"),
				pCI->Name, pCI->bEnable == 1? _T("yes"):_T("no"), 10 * pCI->Scope.MinArea, 10 * pCI->Scope.MaxArea, pCI->Scope.MinSpeed, pCI->Scope.MaxSpeed );
			fRet = _request_cgicommand(_VCA_CGI_, str.GetBuffer(), pcbresponse, &dwresponse );
			str.ReleaseBuffer();
			if (fRet == FALSE)
				AfxMessageBox(_T("_request_cgicomand failed!"));
			iHandledCls ++;
		}
		else if (pOriCI != NULL && pCI != NULL)
		{
			str.Format(_T("action=remove&group=VCA.ch0.cg0.oc%d"),i );
			fRet = _request_cgicommand(_VCA_CGI_, str.GetBuffer(), pcbresponse, &dwresponse );
			str.ReleaseBuffer();
			if (fRet == FALSE)
				AfxMessageBox(_T("_request_cgicomand failed!"));
			iHandledCls ++;	
		}

	}

	str.Format(_T("OK %d:%d"), iHandledCls, m_TotalClass);
	AfxMessageBox(str);
}

BOOL CClassHelperDlg::_request_cgicommand(LPTSTR _lpszcgi, LPTSTR _lpszoption, BYTE* _lpcbresponse, DWORD* _pdwlength )
{
	CHttpClientBasic *pChttp = new CHttpClientBasic();
	pChttp->Init();

	TCHAR szWebServerIP[16] = _T("192.168.1.208");
	INT32 nPort = 80;
	TCHAR szUsername[32] = _T("root");
	TCHAR szPassowrd[32] = _T("pass");

	HANDLE hSession = NULL;
	hSession = pChttp->Connect(m_WebServerIP.GetBuffer(), m_Port, m_Username.GetBuffer(), m_Password.GetBuffer());
	m_WebServerIP.ReleaseBuffer();
	m_Username.ReleaseBuffer();
	m_Password.ReleaseBuffer();

	BOOL fRet = TRUE;
	HANDLE hRequest = pChttp->OpenRequest(hSession, m_bSSL, TRUE, _lpszcgi);
	fRet = pChttp->SendRequest(hRequest, _lpszoption, _lpcbresponse, _pdwlength);

	if (fRet == FALSE)
	{
		AfxMessageBox(_T("Faild to connect webserver!"));
	}

	pChttp->CloseRequest(hRequest);
	if (hSession)
		pChttp->Disconnect(hSession);
	if(pChttp != NULL)
	{
		pChttp->Deinit();
		delete pChttp;
	}
	return fRet;
}

ClassInfo* CClassHelperDlg::FindClassPostion(ClassInfo* pHead, INT32 Postion)
{
	ClassInfo* pCI = pHead;
	while( pCI != NULL)
	{
		if ( pCI->Postion != Postion )
			pCI = pCI->Next;
		else
			return pCI;
	}
	return NULL;
}

ClassInfo* CClassHelperDlg::FindClassID(ClassInfo* pHead, INT32 ID)
{
	ClassInfo* pCI = pHead;
	while( pCI != NULL)
	{
		if ( pCI->ClassID != ID )
			pCI = pCI->Next;
		else
			return pCI;
	}
	return NULL;
}

void CClassHelperDlg::OnClickedBtnMoveup()
{
	// TODO: Add your control notification handler code here
	if ( m_pCurSelClass != NULL)
	{
		INT32 TmpPos;
		ClassInfo* pCI;

		pCI = FindClassPostion(m_pHeadClass, m_pCurSelClass->Postion - 1 );
		if ( pCI == NULL )
			return;

		TmpPos = m_pCurSelClass->Postion;
		m_pCurSelClass->Postion -= 1;
		pCI->Postion = TmpPos;

		CString szStr;
		szStr.Format(_T("%5d         %s"), m_pCurSelClass->ClassID, m_pCurSelClass->Name);
		m_ListClass.DeleteString(TmpPos);
		m_ListClass.InsertString(m_pCurSelClass->Postion, szStr);
		Invalidate();
		UpdateWindow();
	}
}


void CClassHelperDlg::OnClickedBtnMovedown()
{
	// TODO: Add your control notification handler code here
	if ( m_pCurSelClass != NULL)
	{
		INT32 TmpPos;
		ClassInfo* pCI;

		pCI = FindClassPostion(m_pHeadClass, m_pCurSelClass->Postion + 1 );
		if ( pCI == NULL )
			return;

		TmpPos = m_pCurSelClass->Postion;
		m_pCurSelClass->Postion += 1;
		pCI->Postion = TmpPos;

		CString szStr;
		szStr.Format(_T("%5d         %s"), m_pCurSelClass->ClassID, m_pCurSelClass->Name);
		m_ListClass.DeleteString(TmpPos);
		m_ListClass.InsertString(m_pCurSelClass->Postion, szStr);
		Invalidate();
		UpdateWindow();
	}
}


void CClassHelperDlg::OnClickedCheckSsl()
{
	// TODO: Add your control notification handler code here
	if (BST_CHECKED == ((CButton*)GetDlgItem(IDC_CHECK_SSL))->GetCheck())
		((CEdit*)GetDlgItem(IDC_EDIT_PORT))->SetWindowText(_T("443"));
	else
		((CEdit*)GetDlgItem(IDC_EDIT_PORT))->SetWindowText(_T("80"));

}
