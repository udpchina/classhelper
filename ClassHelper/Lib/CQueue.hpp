#ifndef __CQUEUECLASS_HPP___
#define __CQUEUECLASS_HPP___

class CQue
{
public:
	#define INCREMENT_RZ(a,b)   (((a+1) >= (b)) ? 0 : (a+1))
	#define	CQUE_COUNT			(128)
/*
#if USING_COD5_BUFFER
	#define CQUECELL_MAXLENGTH	(1*1024)
#else
	#define CQUECELL_MAXLENGTH	(512*1024)
#endif
*/
	typedef struct _tagQUEBUF
	{
		DWORD	dwLength;
		LPBYTE  lpBuff;
	}QUEBUF;

	typedef struct _tagSTATICQUEBUF
	{
		DWORD	dwLength;
		BYTE    * acbBuff;
		DWORD	dwBufSize;
//		BYTE    acbBuff[CQUECELL_MAXLENGTH];
	}STATICQUEBUF;

public:
	CQue();
	~CQue();

	HRESULT m_InQue( QUEBUF* _pqueBuf1, QUEBUF* _pqueBuf2 );
	HRESULT m_OutQue( QUEBUF* _pqueBuf, DWORD* _pdwTruncatedLength );
	STATICQUEBUF* m_LockQue();
	VOID m_UnlockQue( STATICQUEBUF* _ptQueBuf );

	INT		m_GetQueCount();
	INT		m_PeekQueTailDataLength();
	INT 	m_IsQueFull();		// 1 : Full, 0 : No Full
	INT 	m_IsQueEmpty();		// 1 : Empty, 0 : No Empty
	HRESULT m_FlushAllQue();

#ifdef _VT
	INT m_GetVideoInQueCount();
#endif

private:

	STATICQUEBUF	m_aQueBuf[CQUE_COUNT];		// Ring Que
	INT		m_nHead;
	INT		m_nTail;
	INT		m_nCount;

	CRITICAL_SECTION	m_cs;
	CRITICAL_SECTION	m_csAccess;
};

#endif	//#ifndef __CQueCLASS_HPP___
