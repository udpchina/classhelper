#include "StdAfx.h"

#ifndef _AFXDLL
#include <windows.h>
#endif

#include "CQueue.hpp"

#ifndef _AFXDLL
#include "Misc.h"
#endif

CQue::CQue()
{
	ZeroMemory( m_aQueBuf, sizeof(STATICQUEBUF) * CQUE_COUNT );

	m_nHead = m_nTail = 0;
	m_nCount = 0;

	for( INT i=0; i<CQUE_COUNT; i++ ) {
		m_aQueBuf[i].dwBufSize = 0;
		m_aQueBuf[i].acbBuff = NULL;
	}

	InitializeCriticalSection( &m_cs );
	InitializeCriticalSection( &m_csAccess );

	//ErrDbg(1, "CQue 생성자 (%Xh)\r\n", this);
}

CQue::~CQue()
{
	m_FlushAllQue();

	EnterCriticalSection( &m_cs );
	m_nCount = 0;
	LeaveCriticalSection( &m_cs );

	DeleteCriticalSection( &m_csAccess );
	DeleteCriticalSection( &m_cs );

	for( INT i=0; i<CQUE_COUNT; i++ ) {
		m_aQueBuf[i].dwLength = 0;
		m_aQueBuf[i].dwBufSize = 0;
	}

	ErrDbg(1, _T("CQue 소멸자\r\n"));
}

HRESULT CQue::m_InQue( QUEBUF* _pqueBuf1, QUEBUF* _pqueBuf2 )
{
	//
	// 여러 thread에서 공유하는 m_nCount, m_nHead, m_nTail값은 주의한다.
	//
	HRESULT hr = 0;

	EnterCriticalSection( &m_cs );
	do {
		if( (m_nCount + 1) > CQUE_COUNT )
		{	// Que가 모자란다.	QueFull이다.
			hr = -3;
			break;
		}
	}while(0);
	LeaveCriticalSection( &m_cs );

	if( FAILED(hr) ) {
		return hr;
	}

	// 여기 외에는 m_nHead값을 건들이지 않는다. (보호할 필요가 있는것은 m_nCount)

	//ErrDbg(1, "InQue %Xh %d \r\n", _pqueBuf->lpBuff, _pqueBuf->dwLength);
	DWORD	dwLength;
	PBYTE	pBytes;
	DWORD	dwTotalLength;

	if(NULL != _pqueBuf2)
		dwTotalLength = _pqueBuf1[0].dwLength + _pqueBuf2[0].dwLength;
	else
		dwTotalLength = _pqueBuf1[0].dwLength;

	if(m_aQueBuf[m_nHead].acbBuff==NULL){
		m_aQueBuf[m_nHead].acbBuff = (BYTE *)malloc(dwTotalLength);
		m_aQueBuf[m_nHead].dwBufSize = dwTotalLength;
	}else if(m_aQueBuf[m_nHead].dwBufSize < dwTotalLength){
		free( m_aQueBuf[m_nHead].acbBuff );
		m_aQueBuf[m_nHead].acbBuff = (BYTE *)malloc(dwTotalLength);
		m_aQueBuf[m_nHead].dwBufSize = dwTotalLength;
	}


	pBytes   = m_aQueBuf[m_nHead].acbBuff;
	dwLength = _pqueBuf1[0].dwLength;

	CopyMemory( pBytes, _pqueBuf1[0].lpBuff, dwLength );
	m_aQueBuf[m_nHead].dwLength = dwLength;
	pBytes += dwLength;

	if( NULL != _pqueBuf2 ) {
		dwLength = _pqueBuf2[0].dwLength;

		CopyMemory( pBytes, _pqueBuf2[0].lpBuff, dwLength );
		m_aQueBuf[m_nHead].dwLength += dwLength;
	}

	EnterCriticalSection( &m_cs );
	m_nHead = INCREMENT_RZ( m_nHead, CQUE_COUNT );
	m_nCount += 1;
	LeaveCriticalSection( &m_cs );

	return hr;
}

CQue::STATICQUEBUF* CQue::m_LockQue()
{
	EnterCriticalSection( &m_cs );
	STATICQUEBUF*	ptRet = NULL;
	if( m_nCount > 0 ) {
		ptRet = &m_aQueBuf[m_nTail];
	}
	LeaveCriticalSection( &m_cs );
	
	return ptRet;
}

VOID CQue::m_UnlockQue( STATICQUEBUF* _ptQueBuf )
{
	if( _ptQueBuf == NULL ) return;

	EnterCriticalSection( &m_cs );
	_ptQueBuf->dwLength = 0;
	if (m_nCount > 0) {
		m_nCount -= 1;
	}
	m_nTail = INCREMENT_RZ(m_nTail, CQUE_COUNT);
	LeaveCriticalSection( &m_cs );
}

HRESULT CQue::m_OutQue( QUEBUF* _pqueBuf, DWORD* _pdwTruncatedLength )
{
	HRESULT hr = 0;

	EnterCriticalSection( &m_cs );
	if( m_nCount < 1 )
	{	// 너무 많은 OutQue를 요청. 또는 QueEmpty
		LeaveCriticalSection( &m_cs );
		return -3;
	}
	LeaveCriticalSection( &m_cs );

	DWORD	dwLength;

	//ErrDbg(1, "OutQue %Xh %d \r\n", _pqueBuf->lpBuff, _pqueBuf->dwLength);

	dwLength = m_aQueBuf[m_nTail].dwLength;
	dwLength = min( dwLength, _pqueBuf->dwLength );
	CopyMemory( _pqueBuf->lpBuff, m_aQueBuf[m_nTail].acbBuff, dwLength );
	_pqueBuf->dwLength = dwLength;
	*_pdwTruncatedLength = m_aQueBuf[m_nTail].dwLength - dwLength;

	m_aQueBuf[m_nTail].dwLength = 0;

	EnterCriticalSection( &m_cs );
	m_nCount -= 1;
	m_nTail = INCREMENT_RZ(m_nTail, CQUE_COUNT);
	LeaveCriticalSection( &m_cs );

	return hr;
}

#ifdef _VT
#include "nvccommon.h"
INT CQue::m_GetVideoInQueCount()
{
	EnterCriticalSection( &m_cs );

	INT iVidCount = 0;

	int i = m_nTail;
	while( i != m_nHead )
	{
		tSTREAMHDR*	pHdr = (tSTREAMHDR *)m_aQueBuf[i].acbBuff;

		if( pHdr->cStreamType < 5 )
		{
			// Video
			iVidCount++;
		}

		i = INCREMENT_RZ( i, CQUE_COUNT );
	}

	LeaveCriticalSection( &m_cs );

	return iVidCount;
}
#endif // _VT

INT CQue::m_GetQueCount()
{
	return m_nCount;
}

INT CQue::m_PeekQueTailDataLength()
{
	return (INT)( m_aQueBuf[m_nTail].dwLength );
}

INT CQue::m_IsQueFull()
{
	return ( m_nCount == CQUE_COUNT ) ? 1 : 0;
}

INT CQue::m_IsQueEmpty()
{
	return ( m_nCount == 0 ) ? 1 : 0;
}

HRESULT CQue::m_FlushAllQue()
{
	EnterCriticalSection( &m_cs );

	for( INT i=0; i < CQUE_COUNT; i++ )
	{
		if( m_aQueBuf[i].acbBuff ) {
			free(m_aQueBuf[i].acbBuff);
			m_aQueBuf[i].acbBuff = NULL;
			m_aQueBuf[i].dwLength = 0;
		}
	}

	m_nHead = m_nTail = 0;
	m_nCount = 0;

	LeaveCriticalSection( &m_cs );
	return 0;
}
