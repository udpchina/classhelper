#ifndef _PARSING_H_
#define _PARSING_H_

// linked list
struct tLL {
	char*		pszleft;
	char*		pszright;

	struct tLL*	ptend;
	struct tLL*	ptnext;
};

int	parsing_linkedlist_delete( struct tLL* _ptLLTop, const char* _pszleft );
struct tLL*	parsing_linkedlist_findleft( struct tLL* _ptLL, const char* _pszleft );
struct tLL*	parsing_linkedlist_insertafter( struct tLL* _ptLL, const char* _pszleft, const char* _pszright );
int parsing_linkedlist_cleanall( struct tLL* _ptLL );
int parsing_linkedlist2writefile( struct tLL* _ptLLTop, char* _pszfilename );

int parsing_readfile2linkedlist( char* _pszfilename, unsigned char* _pszdelimit, struct tLL* _ptLLTop );
int parsing_readbuffer2linkedlist( char* _pszbuffer, unsigned char* _pszdelimit, struct tLL* _ptLLTop );

int parsing_writefile( IN const void* _pcbdata,
					   IN size_t	  _nlength,
					   IN const char* _pszfilename );

int	parsing_gettokenvalue( IN  char*  _psz,
						   IN  char*  _pszdelimit, 
						   OUT char	  _aszvalue[][128],
						   IN OUT int*	  _pncount );


#endif	//#ifndef _PARSING_H_
