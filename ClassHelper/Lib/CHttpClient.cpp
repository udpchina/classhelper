
#include "StdAfx.h"

#ifndef _AFXDLL
#include <windows.h>
#endif

#ifndef _AFXDLL
#include "Misc.h"
#endif

#include "CHttpClient.hpp"
#include "atlconv.h"

#pragma warning(disable:4127)  // warning C4127: conditional expression is constant
#pragma warning(disable:4702)  // warning C4702: unreachable code

/*************************** CHttpClientBasic ********************************/
CHttpClientBasic::CHttpClientBasic()
{
	m_hInet = NULL;
	m_eventError = NULL;
}

CHttpClientBasic::~CHttpClientBasic()
{
}

DWORD CHttpClientBasic::GetLastError( OUT LPTSTR _lpszErrName )
{
	lstrcpy( _lpszErrName, m_szLastErrorName );
	return m_dwLastErrorCode;
}

HRESULT CHttpClientBasic::SetupLastErrorEvent( HANDLE _hErrorEvent )
{
	m_eventError = _hErrorEvent;
	return 0;
}

HRESULT CHttpClientBasic::Init()
{
	HRESULT	hRet = NO_ERROR;

	HINTERNET	hInet = NULL;

	do {
		hInet = InternetOpenA( "CLIENT_NVC",
							  INTERNET_OPEN_TYPE_PRECONFIG,
							  NULL, //LPCTSTR lpszProxyName,
							  NULL, //LPCTSTR lpszProxyBypass,
							  0 ); //INTERNET_FLAG_ASYNC );
		if( NULL == hInet ) {
			hRet = -1;
			break;
		}
	}while(0);

	if( NO_ERROR == hRet ) {
		m_hInet = hInet;
	}
	else {
		if(hInet) {
			InternetCloseHandle(hInet);
		}
	}

	return hRet;
}

HRESULT CHttpClientBasic::Deinit()
{
	HRESULT	hRet = NO_ERROR;

	if( m_hInet ) {
		InternetCloseHandle(m_hInet);
	}
	m_hInet = NULL;

    return hRet;
}

HANDLE CHttpClientBasic::Connect( LPTSTR	_lpszIP,
							 INT	_nPort,
							 LPTSTR	_lpszUsername,
							 LPTSTR	_lpszPassword )
{
	if( NULL == m_hInet ) return NULL;

	HRESULT		hRet = NO_ERROR;
	HINTERNET	hSession = NULL;

	do {
		hSession = InternetConnect( m_hInet,
								    _lpszIP,					// server name
									(INTERNET_PORT)_nPort,		// server port
									_lpszUsername,
									_lpszPassword,
									INTERNET_SERVICE_HTTP,
									0,
									NULL );
		if( NULL == hSession ) {
			hRet = -2;
			break;
		}
	}while(0);

	if( NO_ERROR == hRet ) {
		;
	}
	else {
		if(hSession) {
			InternetCloseHandle(hSession);
		}
		hSession = NULL;
	}

	return hSession;
}

HRESULT CHttpClientBasic::Disconnect( HANDLE _hSession )
{
	if( _hSession ) {
		InternetCloseHandle(_hSession);
	}
	return 0;
}

HANDLE CHttpClientBasic::OpenRequest( HANDLE _hSession,
								 BOOL	_fIsHTTPS,
								 BOOL	_fIsIgnoreCA,
								 LPTSTR _lpszObject,
								 BOOL	_fUseCache )
{
	if( NULL == _hSession ) return NULL;
	if( NULL == _lpszObject ) return NULL;

	BOOL		fRet;
	HINTERNET	hRequest = NULL;

	LPTSTR	apszMIME[] = { _T("text/*"), NULL };

	DWORD	dwFlag = 0;

	if( _fIsHTTPS ) {
		dwFlag = INTERNET_FLAG_SECURE;

		if( _fIsIgnoreCA ) {
			dwFlag |= INTERNET_FLAG_IGNORE_CERT_CN_INVALID;
		}
	}


	if( !_fUseCache )
	{
		// VT: Sometimes things are cached (e.g. like snapshot!)
		dwFlag |= INTERNET_FLAG_DONT_CACHE; 
	}

	hRequest = HttpOpenRequest( _hSession,
								_T("POST"),				//org:"POST", [modify] if .fcgi then "GET"
								_lpszObject,
								NULL,
								NULL,
								(LPCTSTR*)apszMIME,
								dwFlag,
								NULL );
	if( NULL == hRequest ) {
		return NULL;
	}

	DWORD	adwFlag[1] = {0,};
	DWORD	dwFlagBufferLength;
	if( _fIsIgnoreCA ) {
		dwFlagBufferLength = sizeof(adwFlag);
		fRet = InternetQueryOptionA( hRequest, 
									INTERNET_OPTION_SECURITY_FLAGS,
									adwFlag, 
									&dwFlagBufferLength );
		if( TRUE == fRet ) {
			adwFlag[0] |= INTERNET_OPTION_SECURITY_FLAGS | SECURITY_FLAG_IGNORE_UNKNOWN_CA;
			dwFlagBufferLength = 4;
			fRet = InternetSetOption( hRequest, 
									  INTERNET_OPTION_SECURITY_FLAGS,
									  adwFlag, 
									  dwFlagBufferLength );
		}
	}

	// limited connections
	adwFlag[0] = 10;
	dwFlagBufferLength = sizeof(adwFlag);
	fRet = InternetSetOption( hRequest, 
							  INTERNET_OPTION_MAX_CONNS_PER_1_0_SERVER,
							  adwFlag, 
							  dwFlagBufferLength );

	adwFlag[0] = 10;
	dwFlagBufferLength = sizeof(adwFlag);
	fRet = InternetSetOption( hRequest, 
							  INTERNET_OPTION_MAX_CONNS_PER_SERVER,
							  adwFlag, 
							  dwFlagBufferLength );

	return hRequest;
}

HRESULT CHttpClientBasic::CloseRequest( HANDLE _hOpenRequest )
{
	if( _hOpenRequest ) {
		InternetCloseHandle(_hOpenRequest);
	}
	return 0;
}

BOOL CHttpClientBasic::SendRequest( HANDLE _hOpenRequest, 
							   LPTSTR _lpszOption,
							   BYTE*  _lpcbResponse,
							   DWORD* _lpdwResponseLength )
{
	if( NULL == _hOpenRequest ) return FALSE;
	if( NULL == _lpcbResponse ) return FALSE;
	if( NULL == _lpdwResponseLength ) return FALSE;

	BOOL	fRet;
	BYTE*   lpcbResponse;
	USES_CONVERSION;

	lpcbResponse = _lpcbResponse;

	DWORD dwOptionLength = 0;
	if( _lpszOption ) {
		dwOptionLength = lstrlen(_lpszOption);
	}

	fRet = HttpSendRequest( _hOpenRequest, NULL, 0, T2A(_lpszOption), dwOptionLength );

	if( FALSE == fRet ) {
		m_dwLastErrorCode = ::GetLastError();
		lstrcpy( m_szLastErrorName, _T("HttpSendRequest") );
		SetEvent( m_eventError );
	}
	else {
		DWORD	dwTotalReadedBytes = 0;
		DWORD	dwTotalQueryLength = 0;

		do {
			DWORD	dwQueryLength;
			DWORD	dwReadedBytes;

			InternetQueryDataAvailable( _hOpenRequest, &dwQueryLength, 0, 0 );
			dwTotalQueryLength += dwQueryLength;

			if( *_lpdwResponseLength < dwTotalQueryLength ) {
				*_lpdwResponseLength = dwTotalQueryLength;
				fRet = FALSE;
				break;
			}

			if( 0 != dwQueryLength ) {
				lpcbResponse = _lpcbResponse + dwTotalReadedBytes;
				InternetReadFile( _hOpenRequest, lpcbResponse, dwQueryLength, &dwReadedBytes );
				dwTotalReadedBytes += dwReadedBytes;

				if( 0 == dwReadedBytes ) {
					break;
				}
			}
			else {
				break;
			}
		}while(1);

		*_lpdwResponseLength = dwTotalReadedBytes;
	}

	return fRet;
}

BOOL CHttpClientBasic::SendRequest_Content_Type_APP_Form( HANDLE _hOpenRequest, 
							   LPTSTR _lpszOption,
							   BYTE*  _lpcbResponse,
							   DWORD* _lpdwResponseLength )
{
	if( NULL == _hOpenRequest ) return FALSE;
	if( NULL == _lpcbResponse ) return FALSE;
	if( NULL == _lpdwResponseLength ) return FALSE;

	BOOL	fRet;
	BYTE*   lpcbResponse;

	lpcbResponse = _lpcbResponse;

	DWORD dwOptionLength = 0;
	if( _lpszOption ) {
		dwOptionLength = lstrlen(_lpszOption);
	}

	fRet = HttpSendRequest( _hOpenRequest, _T("Content-Type: application/x-www-form-urlencoded"), lstrlen(_T("Content-Type: application/x-www-form-urlencoded")), _lpszOption, dwOptionLength );
	if( FALSE == fRet ) {
		m_dwLastErrorCode = ::GetLastError();
		lstrcpy( m_szLastErrorName, _T("HttpSendRequest") );
		SetEvent( m_eventError );
	}
	else {
		DWORD	dwTotalReadedBytes = 0;
		DWORD	dwTotalQueryLength = 0;

		do {
			DWORD	dwQueryLength;
			DWORD	dwReadedBytes;

			InternetQueryDataAvailable( _hOpenRequest, &dwQueryLength, 0, 0 );
			dwTotalQueryLength += dwQueryLength;

			if( *_lpdwResponseLength < dwTotalQueryLength ) {
				*_lpdwResponseLength = dwTotalQueryLength;
				fRet = FALSE;
				break;
			}

			if( 0 != dwQueryLength ) {
				lpcbResponse = _lpcbResponse + dwTotalReadedBytes;
				InternetReadFile( _hOpenRequest, lpcbResponse, dwQueryLength, &dwReadedBytes );
				dwTotalReadedBytes += dwReadedBytes;

				if( 0 == dwReadedBytes ) {
					break;
				}
			}
			else {
				break;
			}
		}while(1);

		*_lpdwResponseLength = dwTotalReadedBytes;
	}

	return fRet;
}

/***************************** CHttpClient ***********************************/
CHttpClient::CHttpClient()
{
	m_eventStartQuery = NULL;
	m_threadQuery = NULL;
	m_dwthreadQuery = 0;

	m_eventVQue = NULL;
	m_eventAQue = NULL;

	m_hAVStreamOpenRequest = NULL;
	_CreateQueryThreadAndEvent((ULONG)this);
}

CHttpClient::~CHttpClient()
{
	_DestroyQueryThreadAndEvent();
}

BOOL CHttpClient::AVStreamCancel()
{
	if( m_hAVStreamOpenRequest ) {
		InternetCloseHandle(m_hAVStreamOpenRequest);
	}
	m_hAVStreamOpenRequest = NULL;
	m_eventVQue = NULL;
	m_eventAQue = NULL;

	return TRUE;
}

BOOL CHttpClient::AVStreamRequest( HANDLE _hOpenRequest, LPTSTR _lpszOption, HANDLE _hVQueEvent, HANDLE _hAQueEvent )
{
	if( NULL == _hOpenRequest ) return FALSE;

	BOOL  fRet;
	DWORD dwOptionLength = 0;

	if( _lpszOption ) {
		dwOptionLength = lstrlen(_lpszOption);
	}

	fRet = HttpSendRequest( _hOpenRequest, NULL, 0, _lpszOption, dwOptionLength );
	if( FALSE == fRet ) {
		m_dwLastErrorCode = ::GetLastError();
		lstrcpy( m_szLastErrorName, _T("HttpSendRequest") );
		SetEvent( m_eventError );
	}
	else {
		m_eventVQue = _hVQueEvent;
		m_eventAQue = _hAQueEvent;

		m_hAVStreamOpenRequest = _hOpenRequest;
		SetEvent(m_eventStartQuery);
	}

	return fRet;
}

VOID CHttpClient::_CreateQueryThreadAndEvent( ULONG _ulContextPtr )
{
	m_eventStartQuery = CreateEvent( NULL, FALSE, FALSE, 0 );
	m_threadQuery = 
		CreateThread( NULL, 0,
					  (LPTHREAD_START_ROUTINE)_lpthreadQuery,
					  (LPVOID)_ulContextPtr,
					  THREAD_TERMINATE | THREAD_SET_CONTEXT,
					  &m_dwthreadQuery );
}

VOID CHttpClient::_DestroyQueryThreadAndEvent()
{
	if( 0 != m_dwthreadQuery ) {
		m_dwthreadQuery = 0;
		if( m_eventStartQuery ) SetEvent( m_eventStartQuery );

		DWORD dwObject = WaitForSingleObject( m_threadQuery, 2000 );
		if( dwObject == WAIT_TIMEOUT ) {
			TerminateThread( m_threadQuery, 0x2222CC02 );
		}
		CloseHandle( m_threadQuery );
		m_threadQuery = NULL;

		if( m_eventStartQuery ) CloseHandle( m_eventStartQuery );
		m_eventStartQuery = NULL;
	}
}

LPTHREAD_START_ROUTINE CHttpClient::_lpthreadQuery( LPVOID _pParam )
{
	CHttpClient*	pcThis = (CHttpClient*)_pParam;

	BOOL	fRet;
	DWORD	dwEventObject;

	HINTERNET hRequest;

	DWORD	dwBufferLength = 1*1024*1024;
	BYTE*	pcbBuffer = new BYTE[dwBufferLength];

	DWORD	dwTotalStreamLength = 0;
	CQue::QUEBUF	tQue = {0,};
	tSTREAMHDR*		pHdr = NULL;

	// imsi code for testtest....
	//CQue::STATICQUEBUF*	ptSQue = NULL;

	__try
	{
		while( pcThis->m_dwthreadQuery ) {
			dwEventObject = WaitForSingleObject( pcThis->m_eventStartQuery, INFINITE );

			if( dwEventObject == (WAIT_OBJECT_0 + 0) ) {
				if( 0 == pcThis->m_dwthreadQuery ) {
					//ErrDbgX(1, "~~~~~~~ Destroy Thread ~~~~~~\n");
					break;
				}
				//ErrDbgX(1, "~~~~~~~ Start Query Event ~~~~~~\n");

				DWORD	dwTotalReadedBytes = 0;
				DWORD	dwTotalQueryLength = 0;
				DWORD	dwQueryLength;
				DWORD	dwReadedBytes;
				BYTE*	pcbTempBuffer = pcbBuffer;

				INT		nStreamType = 0;	// 0:undefine, 1:video, 2:audio

				do {
					if( NULL == pcThis->m_hAVStreamOpenRequest ) {
						break;
					}

					hRequest = pcThis->m_hAVStreamOpenRequest;
					fRet = InternetQueryDataAvailable( hRequest, &dwQueryLength, 0, 0 );
					if( FALSE == fRet ) {
						pcThis->m_dwLastErrorCode = ::GetLastError();
						lstrcpy( pcThis->m_szLastErrorName, _T("InternetQueryDataAvailable") );
						SetEvent( pcThis->m_eventError );

						if( 998 == pcThis->m_dwLastErrorCode ) {
							continue;
						}
						break;
					}
					dwTotalQueryLength += dwQueryLength;

					// 임시 버퍼 크기 보다 더 큰 것이 온다????
					if( dwBufferLength < dwTotalQueryLength ) {
						ErrDbgX(1, _T("OOOPS!!! too big stream data dwQueryLength %d \r\n"), dwQueryLength);
						break;
					}

					if( 0 != dwQueryLength ) {
						pcbTempBuffer = pcbBuffer + dwTotalReadedBytes;

						fRet = InternetReadFile( hRequest, pcbTempBuffer, dwQueryLength, &dwReadedBytes );
						if( FALSE == fRet ) {
							pcThis->m_dwLastErrorCode = ::GetLastError();
							lstrcpy( pcThis->m_szLastErrorName, _T("InternetReadFile") );
							SetEvent( pcThis->m_eventError );
							break;
						}
						if( 0 == dwReadedBytes ) {
							// EOF
							break;
						}

						dwTotalReadedBytes += dwReadedBytes;

						// query data는 쪼개져서 들어온다. 시작이 어디고, 끝은 어디일까?
						// stream data가 전부 왔는지 확인한다.
						// 또한 stream data가 여러개 넘쳐 있을 수 있다.

						pHdr = (tSTREAMHDR*)pcbBuffer;

			label_check:
						// 1. stream 시작인지 본다.
						if( (0x11AFCAA9 == pHdr->nMagicID1) && 
							(0xEE102FBD == pHdr->nMagicID2)	   ) {


#ifdef __USE_CUSTOM_DSP_HDR__
							if ( CUSTOM_META_STREAM_TYPE == pHdr->cStreamType )
							{
								// Metadata (VCAmeta or MDmeta )
								nStreamType =3;
							}
							else
#endif // __USE_CUSTOM_DSP_HDR__
							// detect video or audio packet?
							if( 128 > pHdr->cStreamType ) {
								// video
								nStreamType = 1;
							}
							else {
								// audio
								nStreamType = 2;
							}

							dwTotalStreamLength = pHdr->nLength + pHdr->nStructSize;
							//ErrDbgX(1, "~~(%d)BEGIN %d/%d \r\n", nStreamType, dwTotalReadedBytes, dwTotalStreamLength);
						}

						// 2. stream 끝인지 본다.
						if( dwTotalStreamLength == dwTotalReadedBytes ) {

							// Queing...
							tQue.lpBuff   = pcbBuffer;
							tQue.dwLength = dwTotalStreamLength;
							if( 1 == nStreamType ) {
								// video
								pcThis->m_cVQue.m_InQue( &tQue, NULL );
								if( pcThis->m_eventVQue ) {
									SetEvent( pcThis->m_eventVQue );
								}
							}
#ifdef __USE_CUSTOM_DSP_HDR__
							else if ( 3 == nStreamType ) {
								// Metadata (VCAmeta or MDmeta )
								pcThis->m_cVQue.m_InQue( &tQue, NULL );
								if( pcThis->m_eventVQue ) {
									SetEvent( pcThis->m_eventVQue );
								}
							}
#endif // __USE_CUSTOM_DSP_HDR__
							else {
								// audio
								pcThis->m_cAQue.m_InQue( &tQue, NULL );
								if( pcThis->m_eventAQue ) {
									SetEvent( pcThis->m_eventAQue );
								}
							}

							//ErrDbgX(1, "~~END \r\n");

							nStreamType = 0;
							dwTotalQueryLength  = 0;
							dwTotalReadedBytes  = 0;
							dwTotalStreamLength = 0;
						}

						// 3. 더 받아야 한다.
						if( dwTotalStreamLength > dwTotalReadedBytes ) {
							//ErrDbgX(1, "~~MORE %d \r\n", dwTotalStreamLength - dwTotalReadedBytes);
							continue;
						}

						// 4. stream 끝 보다 더 받았다???
						if( dwTotalStreamLength < dwTotalReadedBytes ) {
							//ErrDbgX(1, "~~OVER ??? tot %d, readed %d \r\n", dwTotalStreamLength, dwTotalReadedBytes);

							// 4.1 넘쳤으니까 일단 stream data를 가져온다.
							// Queing...
							if( 0 != dwTotalStreamLength ) {
								tQue.lpBuff   = pcbBuffer;
								tQue.dwLength = dwTotalStreamLength;
								if( 1 == nStreamType ) {
									pcThis->m_cVQue.m_InQue( &tQue, NULL );
									if( pcThis->m_eventVQue ) {
										SetEvent( pcThis->m_eventVQue );
									}
								}
#ifdef __USE_CUSTOM_DSP_HDR__
								else if ( 3 == nStreamType ) {
									// Metadata (VCAmeta or MDmeta )
									pcThis->m_cVQue.m_InQue( &tQue, NULL );
									if( pcThis->m_eventVQue ) {
										SetEvent( pcThis->m_eventVQue );
									}
								}
#endif // __USE_CUSTOM_DSP_HDR__
								else {
									pcThis->m_cAQue.m_InQue( &tQue, NULL );
									if( pcThis->m_eventAQue ) {
										SetEvent( pcThis->m_eventAQue );
									}
								}
								//ErrDbgX(1, "~~END in OVER \r\n");
							}

							// * system overload가 걸리면 빈번히 발생한다.
							//   다음 stream data와 겹쳐 있는것이다.
							DWORD dwOverBytes  = dwTotalReadedBytes - dwTotalStreamLength;
							dwTotalQueryLength = dwTotalQueryLength - dwTotalStreamLength;

							// 4.2 넘쳐있는 나머지 부분을 정리한다.
							CopyMemory( pcbBuffer,
										(const VOID*)(pcbBuffer + dwTotalStreamLength),
										dwOverBytes );
							ZeroMemory( (pcbBuffer + dwOverBytes), (dwBufferLength - dwOverBytes) );

							// 4.3 확인 가능한 크기가 올때까지 더 받는다.
							if( dwOverBytes < sizeof(tSTREAMHDR) ) {
								//ErrDbgX(1, ":LOOP in OVER readed %d, NEED MORE!! \r\n", dwOverBytes);

								dwTotalReadedBytes  = dwOverBytes;
								dwTotalStreamLength = 0;

								// HTTP로 부터 더 들어오기를 기다린다.
								continue;
							}

							// 4.4 stream data가 여러개 넘쳐 있을 수 있다.
							//     넘쳐 있는 stream data에 MAGIC-ID가 있는지 본다.
							pHdr = (tSTREAMHDR*)pcbBuffer;

							if( (0x11AFCAA9 == pHdr->nMagicID1) && 
								(0xEE102FBD == pHdr->nMagicID2)	   ) {
								//ErrDbgX(1, ":LOOP in OVER readed %d \r\n", dwOverBytes);

								dwTotalReadedBytes  = dwOverBytes;
								dwTotalStreamLength = 0;

								goto label_check;
							}
							else {
								// 넘쳐있다고 했는데, MAGIC-ID가 없다???
								// 데이터가 깨져서 들어온다???

								// 2007-09-27  v0.1.1.0
								// Magic-ID가 없으므로 바로 앞에 InQue 데이터는 깨져서 못쓴다. 버린다.
								//pcThis->m_cVQue.m_FlushAllQue();

								// 2007-09-21  v0.1.0.0
								// MAGIC-ID를 찾아낸다.
								BOOL  fFindMagicID = FALSE;
								DWORD offset;
								for( offset = 0; offset < dwOverBytes; offset += 1 ) {
									if( (offset + 8) >= dwOverBytes ) {
										fFindMagicID = FALSE;
										break;
									}

									pHdr = (tSTREAMHDR*)(pcbBuffer + offset);
									if( (0x11AFCAA9 == pHdr->nMagicID1) && 
										(0xEE102FBD == pHdr->nMagicID2)	   ) {
										fFindMagicID = TRUE;
										break;
									}
								}

								if( TRUE == fFindMagicID ) {
									//ErrDbgX(1, ":FIND MAGICID in OVER offset bytes %d \r\n", offset);

									// 넘쳐있는 나머지 부분을 정리한다.
									DWORD	dwRemain;
									dwRemain = dwOverBytes - offset;
									CopyMemory( pcbBuffer,
												(const VOID*)(pcbBuffer + offset),
												dwRemain );
									ZeroMemory( (pcbBuffer + dwRemain), (dwBufferLength - dwRemain) );

									dwTotalQueryLength  = dwTotalQueryLength - offset;
									dwTotalReadedBytes  = dwRemain;
									dwTotalStreamLength = 0;

									pHdr = (tSTREAMHDR*)pcbBuffer;
									goto label_check;
								}
								else {
									pcThis->m_dwLastErrorCode = 0;
									lstrcpy( pcThis->m_szLastErrorName, _T("MAGIC-ID") );
									SetEvent( pcThis->m_eventError );

									ZeroMemory( pcbBuffer, dwBufferLength );
									dwTotalQueryLength  = 0;
									dwTotalStreamLength = 0;
									dwTotalReadedBytes  = 0;

									nStreamType = 0;

									// 끝내버린다.
									break;
								}
							}
						}// // 4. stream 끝 보다 더 받았다???
					}//if( 0 != dwQueryLength )
					else {
						break;
					}
				}while(1);

				//ErrDbgX(1, "dwTotalQueryLength %d \r\n", dwTotalQueryLength);
				pcThis->AVStreamCancel();

			}//if( dwEventObject == (WAIT_OBJECT_0 + 0) )
		}//while( pcThis->m_dwthreadQuery )
	}//__try

	__except( EXCEPTION_EXECUTE_HANDLER )
    { 
		if( pcbBuffer ) delete[] pcbBuffer;
		pcbBuffer = NULL;

		ExitThread( 0x2222EE01 );
		return 0;
	}

	if( pcbBuffer ) delete[] pcbBuffer;
	pcbBuffer = NULL;

	// warning C4702: unreachable code ???
	ExitThread( 0x22220001 );
	
	return 0;
}//LPTHREAD_START_ROUTINE CHttpClient::_lpthreadQuery( LPVOID _pParam )

