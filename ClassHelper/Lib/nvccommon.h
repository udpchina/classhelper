
#ifndef __NVCCOMMON_H__
#define __NVCCOMMON_H__

#define	IN
#define OUT
#define INOUT

typedef struct _tagAVSTREAMHDR
{
	unsigned int	nStructSize;		// sizeof(tSTREAMHDR)	64Bytes
	unsigned int	nMagicID1;			// 0x11AFCAA9
	unsigned int	nMagicID2;			// 0xEE102FBD

	unsigned int	nLength;
	  signed int	nWidth;
	  signed int	nHeight;

	unsigned char	cStreamType;		// 0 : undefine
										// 1 : MJPEG or JPEG
										// 2 : MPEG2
										// 3 : MPEG4
										// 4 : H.264
	
										// 64 : custom meta data
										
										// 128 : PCM
										// 129 : G.711 (uLaw)
	                                    // 130 : G.711 (aLaw)

	unsigned char	cSubStreamType;		// 0 : undefine
										// 1 :  8000Hz,  8bits, Mono
										// 2 :  8000Hz, 16bits, Mono
										// 3 : 16000Hz,  8bits, Mono
										// 4 : 16000Hz, 16bits, Mono

	unsigned char	cFrameType;			// 0 : undefine
										// 1 : JPEG
										// 2 : I
										// 3 : P
										// 4 : B
										// 5 : A (audio)
										// 6 : M (meta)
	unsigned char	cSeqHigh;
	unsigned char	cSeqLow;
	unsigned char	cReserved1;			// reserved
	unsigned char	cReserved2;			// reserved
	unsigned char	cReserved3;			// reserved

	unsigned int	nTimeStamp1;
	unsigned int	nTimeStamp2;

	unsigned int	nchannelno;			// zerobased
	unsigned int	nstreamno;			// zerobased

	int				reserved[4];

}tSTREAMHDR;

#endif //#ifndef __NVCCOMMON_H__
