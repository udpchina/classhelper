#include "stdafx.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>		/* for malloc, free */
#include <stdarg.h>		/* for strtok */

#define	mybool	bool
#define IN
#define OUT

#include "helper_parsing.h"

int	parsing_linkedlist_delete( struct tLL* _ptLLTop, const char* _pszleft )
{
	mybool	ffind = false;

	struct tLL* ptLLcur  = _ptLLTop;
	struct tLL* ptLLnext = _ptLLTop->ptnext;

	while( ptLLnext ) {
		if( NULL != ptLLnext->pszleft ) {
			if( 0 == strcmpi( ptLLnext->pszleft, _pszleft ) ) {
				ptLLcur->ptnext = ptLLnext->ptnext;

				if( ptLLnext->pszleft )  free( ptLLnext->pszleft );
				if( ptLLnext->pszright ) free( ptLLnext->pszright );
				free( ptLLnext );

				if( NULL == ptLLcur->ptnext ) {
					// 마지막을 삭제하였다면 _ptLLTop->ptend를 갱신해 준다.
					_ptLLTop->ptend = ptLLcur;
				}

				ffind = true;
				break;
			}
		}

		ptLLcur  = ptLLnext;
		ptLLnext = ptLLnext->ptnext;
	}

	return ffind ? 0 : -1;
}

struct tLL*	parsing_linkedlist_findleft( struct tLL* _ptLL, const char* _pszleft )
{
	struct tLL* ptLL = _ptLL;

	while( ptLL ) {
		if( NULL != ptLL->pszleft ) {
			int	nret = strcmpi( _pszleft, ptLL->pszleft );
			//__E(_DBG_TRA, "strcmpi( '%s', '%s' ) ret %d", _pszleft, ptLL->pszleft, nret);
			if( 0 == nret ) {
				return ptLL;
			}
		}
		ptLL = ptLL->ptnext;
	}

	return NULL;
}

struct tLL*	parsing_linkedlist_insertafter( struct tLL* _ptLL, const char* _pszleft, const char* _pszright )
{
	struct tLL*	ptLLnew;

	ptLLnew = (struct tLL*)malloc( sizeof(struct tLL) );
	memset( ptLLnew, 0, sizeof(struct tLL) );

	ptLLnew->pszleft = (char*)malloc( strlen(_pszleft)+1 );
	strcpy( ptLLnew->pszleft, _pszleft );

	// Right string could be NULL
	if ( _pszright ) {
		ptLLnew->pszright = (char*)malloc( strlen(_pszright)+1 );
		strcpy( ptLLnew->pszright, _pszright );
	}
	else {
		ptLLnew->pszright = (char*)malloc( 1 );
		*ptLLnew->pszright = 0;
	}

	//
	_ptLL->ptnext = ptLLnew;

	return ptLLnew;
}

int parsing_linkedlist_cleanall( struct tLL* _ptLL )
{
	if( NULL == _ptLL ) return 0;

	int	nret;
	nret = 0;

	struct tLL* ptLL = _ptLL;
	while( ptLL ) {
		struct tLL* ptTemp = ptLL->ptnext;

		if( ptLL->pszleft )  free( ptLL->pszleft );
		if( ptLL->pszright ) free( ptLL->pszright );
		free( ptLL );

		ptLL = ptTemp;
	}

	return nret;
}

int parsing_linkedlist2writefile( struct tLL* _ptLLTop, char* _pszfilename )
{
	FILE* pfile = fopen( _pszfilename, "w" );
	if( NULL == pfile ) {
		return -1;
	}

	struct tLL* ptLL = _ptLLTop->ptnext;
	while( ptLL ) {
		if( ptLL->pszleft && ptLL->pszright ) {
			fprintf( pfile, "%s=%s\n", ptLL->pszleft, ptLL->pszright );
		}
		ptLL = ptLL->ptnext;
	}

	fclose( pfile );

	return 0;
}

int parsing_readfile2linkedlist( char* _pszfilename, unsigned char* _pszdelimit, struct tLL* _ptLLTop )
{
	int	nret = 0;

	FILE* pfile = fopen( _pszfilename, "r" );
	if( NULL == pfile ) {
		return -1;
	}

	#define	_MAX_CHARPERLINE	1024
	char  szline[_MAX_CHARPERLINE] = {0,};
	char* pszline = NULL;

	struct tLL*	ptEnd = _ptLLTop->ptend;

	while( !feof(pfile) ) {	// 파일 끝까지 읽어들인다.
		pszline = fgets( szline, _MAX_CHARPERLINE, pfile );	// 파일로 부터 한줄('\r')을 읽어들인다.
		if( ferror(pfile) ) {
			nret = -1;
			break;
		}

		/////////////////////////////////////////////////////////////
		// _pszdelimit를 기준으로 왼쪽은 item, 오른쪽은 value로 paring한다.
		const char* pltoken;
		const char* prtoken;

		pltoken = strtok( pszline, (const char*)_pszdelimit );	// left string
		if( NULL == pltoken ) break;

		prtoken = strtok( NULL, "\r\n" );	// right string
		if( NULL == prtoken ) break;

		//__E(_DBG_TRA, "_readfile2linkedlist: item '%s'", pltoken);
		//__E(_DBG_TRA, "_readfile2linkedlist: value '%s'", prtoken);

		/////////////////////////////////////////////////////////////
		// linked list를 만든다.
		struct tLL*	ptLLnew = parsing_linkedlist_insertafter( ptEnd, pltoken, prtoken );
		// tLLTop->ptend는 항상 마지막 node를 가르킨다.
		_ptLLTop->ptend = ptLLnew;

		// 다음 추가 시킬 포인터로 이동
		ptEnd = ptLLnew;
	}

	fclose( pfile );

	return nret;
}

int parsing_readbuffer2linkedlist( char* _pszbuffer, unsigned char* _pszdelimit, struct tLL* _ptLLTop )
{
	int	nret = 0;

	if( NULL == _pszbuffer ) {
		return -1;
	}

	#define	_MAX_CHARPERLINE	1024
	char  szline[_MAX_CHARPERLINE] = {0,};
	char* pszline = NULL;

	struct tLL*	ptEnd = _ptLLTop->ptend;

	while( *_pszbuffer ) {	// 파일 끝까지 읽어들인다.
		int	len = strstr( _pszbuffer, "\r\n" ) - _pszbuffer;
		pszline = strncpy( szline, _pszbuffer, len );		// 파일로 부터 한줄('\r\n')을 읽어들인다.
		pszline[len] = 0;
		_pszbuffer += (len + 2);

		/////////////////////////////////////////////////////////////
		// _pszdelimit를 기준으로 왼쪽은 item, 오른쪽은 value로 paring한다.
		const char* pltoken;
		const char* prtoken;

		pltoken = strtok( pszline, (const char*)_pszdelimit );	// left string
		if( NULL == pltoken ) break;

		prtoken = strtok( NULL, "\r\n" );	// right string
		// BC: right string could be empty
		//if( NULL == prtoken ) break;

		//__E(_DBG_TRA, "_readfile2linkedlist: item '%s'", pltoken);
		//__E(_DBG_TRA, "_readfile2linkedlist: value '%s'", prtoken);

		/////////////////////////////////////////////////////////////
		// linked list를 만든다.
		struct tLL*	ptLLnew = parsing_linkedlist_insertafter( ptEnd, pltoken, prtoken );
		// tLLTop->ptend는 항상 마지막 node를 가르킨다.
		_ptLLTop->ptend = ptLLnew;

		// 다음 추가 시킬 포인터로 이동
		ptEnd = ptLLnew;

	}

	return nret;
}

int	parsing_gettokenvalue( IN	  char*  _psz,
						   IN	  char*  _pszdelimit, 
						   OUT	  char   _aszvalue[][128],
						   IN OUT int*	 _pncount )
{
	int		nret = 0;

	char	sz[1024] = {0,};
	strcpy( sz, _psz );

	int ncount = 0;
	char* psztoken = strtok( sz, _pszdelimit );
	while( psztoken ) {
		if( *_pncount < (ncount+1) ) {
			nret = -1;
			break;
		}

		if( 128-1 < strlen( psztoken ) ) {
			nret = -2;
			break;
		}

		strcpy( &_aszvalue[ncount][0], psztoken );
		ncount += 1;

		psztoken = strtok( NULL, _pszdelimit );
	}

	*_pncount = ncount;
	//__E(DBG_TRA, "libnvc_gettokenvalue ncount %d, ret %d", ncount, nret);

	return nret;
}

int parsing_writefile( IN const void* _pcbdata,
					   IN size_t	  _nlength,
					   IN const char* _pszfilename )
{
	int	nret = 0;

	FILE* pfile = fopen( _pszfilename, "w" );
	if( NULL == pfile ) {
		nret = -1;
	}
	else {
		fwrite( (const void*)_pcbdata, (size_t)1, (size_t)_nlength, pfile );
		fclose( pfile );
	}

	return nret;
}
