// ComboColor.cpp : implementation file
//

#include "stdafx.h"
#include "ClassHelper.h"
#include "ComboColor.h"


// CComboColor

IMPLEMENT_DYNAMIC(CComboColor, CComboBox)

CComboColor::CComboColor()
{

}

CComboColor::~CComboColor()
{
}


BEGIN_MESSAGE_MAP(CComboColor, CComboBox)
	ON_WM_DRAWITEM()
	ON_WM_DRAWITEM()
	ON_WM_KILLFOCUS()
END_MESSAGE_MAP()



// CComboColor message handlers



void CComboColor::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)

{
	// TODO:  Add your code to draw the specified item
	int index = (int)(lpDrawItemStruct->itemID);
	if (index != -1)
	{
		CDC dc;
		dc.Attach(lpDrawItemStruct->hDC);
		CRect rect(&(lpDrawItemStruct->rcItem));
		CBrush brush(m_ColorsHighligh[index]);
		rect.InflateRect(-1, -1);
		dc.FillRect(rect, &brush);
		if (lpDrawItemStruct->itemState & ODS_SELECTED)
			dc.DrawFocusRect(rect);
		CBrush frameBrush(RGB(0, 0, 0));
		dc.FrameRect(rect, &frameBrush);
		rect.InflateRect(-1, -1);

		dc.Detach();
	}

}


void CComboColor::PreSubclassWindow()
{
	// TODO: Add your specialized code here and/or call the base class
	for (int nColors = 0; nColors < m_ColorsHighligh.GetSize(); nColors++)
		// Add a dummy string for every array item so that WM_DRAWITEM message is sent.
		AddString(_T("1"));

	// Select the first color when the control is created.
	SetCurSel(0);
	CComboBox::PreSubclassWindow();
}



void CComboColor::InitColor(COLORREF *pColor, int ColorCount)
{	
	int i;
	for (i = 0; i < ColorCount; i++)
	{
		m_ColorsHighligh.Add(pColor[i]);
		AddString(_T(""));		
	}
}


void CComboColor::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	// TODO: Add your message handler code here and/or call default
	int index = (int)(lpDrawItemStruct->itemID);
	if (index != -1)
	{
		CDC dc;
		dc.Attach(lpDrawItemStruct->hDC);
		CRect rect(&(lpDrawItemStruct->rcItem));
		CBrush brush(m_ColorsHighligh[index]);
		rect.InflateRect(-2, -2);
		dc.FillRect(rect, &brush);
		if (lpDrawItemStruct->itemState & ODS_SELECTED)
			dc.DrawFocusRect(rect);
		CBrush frameBrush(RGB(0, 0, 0));
		dc.FrameRect(rect, &frameBrush);
		rect.InflateRect(-1, -1);
	
		dc.Detach();
	}
	CComboBox::OnDrawItem(nIDCtl, lpDrawItemStruct);
}
